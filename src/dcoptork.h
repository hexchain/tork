/***************************************************************************
 ** $Id: dcoptork.h,v 1.5 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
#ifndef DCOPTORK_H
#define DCOPTORK_H

#include <dcopobject.h>

class DCOPTork : virtual public DCOPObject
{
    K_DCOP
    k_dcop:


    virtual void startEverything() = 0;
    virtual void stopTor() = 0;
    virtual void toggleKDESetting() = 0;
    virtual bool getKDESetting() = 0;
    virtual void anonymousEmail() = 0;
    virtual void anonymousFirefox() = 0;
    virtual void anonymizedFirefox(const QString & url) = 0;
    virtual void anonymizedOpera(const QString & url) = 0;
    virtual void anonymousOpera() = 0;
    virtual void anonymousKonversation() = 0;
    virtual void anonymousGaim() = 0;
    virtual void anonymousPidgin() = 0;
    virtual void anonymousKonsole() = 0;
    virtual void anonymousKopete() = 0;

};

#endif // DCOPDEMOIFACE_H
