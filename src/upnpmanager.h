/***************************************************************************
 * $Id: upnpmanager.h,v 1.7 2009/10/20 20:16:00 hoganrobert Exp $
 *   Copyright (C) 2008 by Robert Hogan                                    *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   Copyright (C) 2005 by Joris Guisson                                   *
 *   joris.guisson@gmail.com                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.             *
 ***************************************************************************/

#ifndef UPNPMANAGER_H
#define UPNPMANAGER_H

#include <qmap.h>
#include <qwidget.h>
#include "upnp/upnprouter.h"
#include "upnp/upnpdescriptionparser.h"
#include "upnp/upnpmcastsocket.h"
#include "upnp/portlist.h"
#include "upnp/forwardportlist.h"
#include "upnp/httprequest.h"

using namespace kt;
using namespace net;

/**
 * Widget for the UPnP pref dialog page.
 */
class UPnPManager : public QWidget
{
	Q_OBJECT

public:
    static UPnPManager* Manager();
	void discover();
	void forward(const net::ForwardPortList & forwardPorts,
                 bool force, bool silent=false);
	void undoForward(const net::ForwardPortList & forwardPorts,
                 bool silent=false);
    bool routersDiscovered(){ return ((!discoveredRouters.isEmpty()) ? true : false);};
    bool silentUpdate(){ return m_silent;};
    QStringList discoveredRoutersNameList();
protected:
	UPnPManager(QWidget* parent = 0, const char* name = 0);
	virtual ~UPnPManager();
private:
    static UPnPManager* manager;

public slots:
	/**
	 * Add a device to the list. 
	 * @param r The device
	 */
	void discovered(kt::UPnPRouter* r);
    void onReplyOK(kt::UPnPRouter*,bt::HTTPRequest* ,const QString&, bool );
    void onReplyError(kt::UPnPRouter*,bt::HTTPRequest* ,const QString&, bool );
signals:
    void routerDiscovered(kt::UPnPRouter*);
    void forwardingError(kt::UPnPRouter*,const QString &, bool );
    void forwardingOK(kt::UPnPRouter*,const QString &, bool );

private:
       void load();
       void unload();
	
private:
	kt::UPnPRouter* def_router;
    kt::UPnPMCastSocket* sock;
    net::ForwardPortList m_forwardPorts;
    QValueList<kt::UPnPRouter*> discoveredRouters;
    bool m_silent;
};

#endif

