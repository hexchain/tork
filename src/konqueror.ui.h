/***************************************************************************
 ** $Id: konqueror.ui.h,v 1.4 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/


void Konq::init()
{
    kcfg_PrivoxyLocation->setEnabled(kcfg_TorkProxy->isEnabled());

    kcfg_KonqHttpProxy->setEnabled(!kcfg_TorkProxy->isEnabled());
    kcfg_KonqHttpsProxy->setEnabled(!kcfg_TorkProxy->isEnabled());
    kcfg_KonqHttpProxyPort->setEnabled(!kcfg_TorkProxy->isEnabled());
    kcfg_KonqHttpsProxyPort->setEnabled(!kcfg_TorkProxy->isEnabled());

}

void Konq::kcfg_TorkProxy_toggled( bool state)
{

    if (state){
        kcfg_KonqHttpProxy->setText("http://localhost");
        kcfg_KonqHttpsProxy->setText("http://localhost");
        kcfg_KonqHttpProxyPort->setValue(8118);
        kcfg_KonqHttpsProxyPort->setValue(8118);
    }

    kcfg_PrivoxyLocation->setEnabled(state);

    kcfg_KonqHttpProxy->setEnabled(!state);
    kcfg_KonqHttpsProxy->setEnabled(!state);
    kcfg_KonqHttpProxyPort->setEnabled(!state);
    kcfg_KonqHttpsProxyPort->setEnabled(!state);

}
