/***************************************************************************
 * $Id: server.ui.h,v 1.30 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
#include "tork.h"
#include "torkconfig.h"
#include "upnpmanager.h"
#include <klocale.h>
#include <kdebug.h>
#include <qtooltip.h>
#include <qpopupmenu.h>
#include <kiconloader.h>

#ifndef EXTERNAL_GEOIP
# include "GeoIP-1.4.0/libGeoIP/GeoIP.h"
#else
# include <GeoIP.h>
#endif

using namespace bt;
using namespace kt;

void MyServer::init()
{

    kcfg_ForwardPorts->setEnabled(false);
    if (UPnPManager::Manager()->routersDiscovered())
        kcfg_ForwardPorts->setEnabled(true);

    MyFamily->setSelectionMode( QListView::Extended );
    MyFamily->setColumnWidthMode(0, QListView::Manual);
    MyFamily->hideColumn(0);
    MyFamily->header()->setResizeEnabled(FALSE, 0);
    MyFamily->setColumnWidthMode(1, QListView::Manual);
    MyFamily->hideColumn(1);
    MyFamily->header()->setResizeEnabled(FALSE, 1);
    MyFamily->setColumnWidthMode(2, QListView::Manual);
    MyFamily->hideColumn(2);
    MyFamily->header()->setResizeEnabled(FALSE, 2);
    MyFamily->setResizeMode( QListView::LastColumn );

    QStringList existingServers = TorkConfig::myFamily();
    QListViewItem *tm;
    QString fp;
    QString nick;
    QString geoipcc;
    QString countryName;

	for ( QStringList::Iterator it = existingServers.begin(); it != existingServers.end(); ++it )
	{
		if ((*it).isEmpty())
			continue;

        fp.setLength(0);
        nick.setLength(0);
        geoipcc = (*it).section("-",0,0);
        fp = (*it).section("-",1,1);
        nick = (*it).section("-",2);
        countryName = GeoIP_country_name[(geoipcc).toInt()];

        tm = new QListViewItem(MyFamily,countryName,geoipcc,fp,nick);
        tm->setPixmap(3,QPixmap(SmallIcon("tork_" + QString(GeoIP_country_code[geoipcc.toInt()]).lower())));


    }

    ExitPolicy->setSorting(-1,false);
    QStringList exitPolicy = TorkConfig::exitPolicy();
	for ( QStringList::Iterator it = exitPolicy.end(); ; --it )
	{
        if ((it == exitPolicy.begin()) && ((*it).isEmpty()))
            break;
       
		if ((*it).isEmpty())
			continue;

        QString policy = (*it).section(" ",0,0);
        QString section2 = (*it).section(" ",1,1);
        QString ip = section2.section(":",0,0);
        QString port = section2.section(":",1,1);
        tm = new QListViewItem(ExitPolicy,policy, ip,port);

        if (it == exitPolicy.begin())
            break;

    }
    ExitPolicy->setSorting(-1,false);

    if (TorkConfig::currentTorVersion().left(3) == "0.1"){
        kcfg_BridgeRelay->setEnabled(false);
    }

    AccountingStart->setCurrentText(TorkConfig::accountingStart());
  
}




void MyServer::kcfg_DefaultServerIP_toggled( bool )
{
/*    if ( kcfg_DefaultServerIP->isChecked()){
        kcfg_ORPort->setEnabled(false);
    }else{
        kcfg_ORPort->setEnabled(true);
    }*/
}


void MyServer::kcfg_MiddleMan_toggled( bool on)
{
    if (on )
        tabWidget->page(2)->setEnabled(false);
    else{
        tabWidget->page(2)->setEnabled(true);
        kcfg_BridgeRelay->setChecked(false);
    }
}


void MyServer::checkBox6_toggled( bool state)
{

    PerformanceOptions->setEnabled(!state);

    if (state){
        kcfg_NumCPUs->setValue(TorkConfig::defaultNumCPUs());
        kcfg_MaxOnionsPending->setValue(TorkConfig::defaultMaxOnionsPending());
        kcfg_AccountingMax->setValue(TorkConfig::defaultAccountingMax());
        AccountingStart->setCurrentText(TorkConfig::defaultAccountingStart());
        kcfg_ShutDownWaitLength->setValue(TorkConfig::defaultShutDownWaitLength());
        kcfg_ConnLimit->setValue(TorkConfig::defaultConnLimit());
    }

}

void MyServer::pushAdd2_clicked()
{
    QString port;
    if (SocksPort->text() == "0")
        port = "*";
    else
        port = SocksPort->text();

    new QListViewItem(ExitPolicy,Policy->text(0), SocksAddress->text(), port);
}


void MyServer::ExitPolicy_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{

    QPopupMenu *menu = new QPopupMenu( ExitPolicy );

    menu->clear();
    menu->insertItem( "Delete Entry", this,SLOT(slotDeleteEntry()) );
    menu->popup( point );

}


void MyServer::slotDeleteEntry( )
{

    QListViewItemIterator it(ExitPolicy, QListViewItemIterator::Selected);
    while ( it.current() ) {
        if (ExitPolicy->isSelected( it.current()))
            delete it.current();
        ++it;
    }
    

}


void MyServer::pushButton2_clicked()
{

    QListViewItemIterator it(MyFamily, QListViewItemIterator::Selected);
    while ( it.current() ) {
        delete it.current();
        ++it;
    }

}


void MyServer::kcfg_BridgeRelay_toggled( bool on)
{
    if (on ){
        tabWidget->page(2)->setEnabled(false);
        kcfg_MiddleMan->setChecked(true);
        kcfg_PublishServerDescriptor->setChecked(true);
    }else{
        tabWidget->page(2)->setEnabled(true);
        kcfg_MiddleMan->setChecked(false);
        kcfg_PublishServerDescriptor->setChecked(false);
    }

}


