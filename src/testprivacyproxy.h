/****************************************************************************
 ** $Id: testprivacyproxy.h,v 1.2 2008/08/20 16:49:31 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of an example program for Qt.  This example
** program may be used, distributed and modified without limitation.
**
*****************************************************************************/

#ifndef _TESTPRIVACYPROXY_H_
#define _TESTPRIVACYPROXY_H_

#include <qsocket.h>
#include <qtextstream.h>
#include <kdebug.h>
#include <kconfigskeleton.h>
#include <qlistview.h>
#include "torkconfig.h"


class TestPrivoxy : public QObject
{
    Q_OBJECT
public:
    TestPrivoxy( );

    virtual ~TestPrivoxy();


    void sendToServer(const QString &string)
    {
        if (!socket)
            return;
        QTextStream os(socket);
        os << string << "\r\n";
    }

public slots:
//     void socketReadyRead();

    void closeConnection()
    {
        socket->close();
        if ( socket->state() == QSocket::Closing ) {
            // We have a delayed close.
            connect( socket, SIGNAL(delayedCloseFinished()),
                    SLOT(socketClosed()) );
        } else {
            // The socket is closed.
            socketClosed();
        }
    }

    void connectTo(const QString &host, Q_UINT16 port);

signals:

    void connectedToPrivacyProxy();
    void privacyProxyConnectionClosed();
    void fatalError();

private slots:


    void socketConnected()
    {
       emit connectedToPrivacyProxy();
    }

    void socketConnectionClosed()
    {
        emit privacyProxyConnectionClosed();
    }

    void socketClosed()
    {
    }

    void socketError( int e )
    {
         if ( e == QSocket::ErrHostNotFound ||
              e == QSocket::ErrConnectionRefused )
            emit fatalError();
    }


private:
    QSocket *socket;
};

#endif //


