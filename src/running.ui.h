/***************************************************************************
 * $Id: running.ui.h,v 1.10 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

//dummy
#include "torkconfig.h"
#include <klocale.h>
#include <qtooltip.h>
#include <qpopupmenu.h>
#include <kdebug.h>

void Running::init()
{

    QStringList existingMaps = TorkConfig::sOCKSBindAddressMany();
    QListViewItem *tm;
	for ( QStringList::Iterator it = existingMaps.begin(); it != existingMaps.end(); ++it )
	{
		if ((*it).isEmpty())
			continue;
        QString entry = (*it).section(":",0,0);
        QString port = (*it).section(":",1,1);

        tm = new QListViewItem(SOCKSBindAddressMany,entry,port);
    }

    QStringList existingSocksPolicies = TorkConfig::sOCKSPolicy();

	for ( QStringList::Iterator it = existingSocksPolicies.begin(); it != existingSocksPolicies.end(); ++it )
	{
		if ((*it).isEmpty())
			continue;
        QString policy = (*it).section(" ",0,0);
        QString section2 = (*it).section(" ",1,1);
        QString entry = section2.section(":",0,0);
        QString port = section2.section(":",1,1);


        tm = new QListViewItem(SOCKSPolicy,policy,entry,port);
    }

    
}
void Running::PushAdd_clicked()
{
    new QListViewItem(SOCKSBindAddressMany,Address->text(), Port->text());
}

void Running::PushAdd2_clicked()
{
    new QListViewItem(SOCKSPolicy,Policy->currentText(),SocksAddress->text(), SocksPort->text());
}


void Running::SOCKSBindAddressMany_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{

    QPopupMenu *menu = new QPopupMenu( SOCKSBindAddressMany );

    menu->clear();
    menu->insertItem( "Delete Entry", this,SLOT(slotDeleteEntry()) );
    menu->popup( point );

}


void Running::slotDeleteEntry( )
{

    QListViewItemIterator it(SOCKSBindAddressMany, QListViewItemIterator::Selected);
    while ( it.current() ) {
        if (SOCKSBindAddressMany->isSelected( it.current()))
            delete it.current();
        ++it;
    }

}


void Running::SOCKSPolicy_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{

    QPopupMenu *menu = new QPopupMenu( SOCKSPolicy );

    menu->clear();
    menu->insertItem( "Delete Entry", this,SLOT(slotDeletePolicyEntry()) );
    menu->popup( point );

}


void Running::slotDeletePolicyEntry( )
{

    QListViewItemIterator it(SOCKSPolicy, QListViewItemIterator::Selected);
    while ( it.current() ) {
        if (SOCKSPolicy->isSelected( it.current()))
            delete it.current();
        ++it;
    }


}


void Running::kcfg_DefaultRunningNormalOptions_toggled( bool state)
{
    NormalOptions->setEnabled(!state);
}



void Running::kcfg_CookieAuthentication_toggled( bool )
{
    if ( kcfg_CookieAuthentication->isChecked()){
        kcfg_HashedControlPassword->setEnabled(false);
    }else{
        kcfg_HashedControlPassword->setEnabled(true);
    }
}


void Running::kcfg_HashedControlPassword_textChanged( const QString & )
{
    if (! kcfg_HashedControlPassword->text().isEmpty()){
        kcfg_CookieAuthentication->setEnabled(false);
    }else{
        kcfg_CookieAuthentication->setEnabled(true);
    }

}


// <connections>
//     <connection>
//         <sender>SOCKSPolicy</sender>
//         <signal>contextMenuRequested(QListViewItem*,const QPoint&amp;,int)</signal>
//         <receiver>Running</receiver>
//         <slot>SOCKSPolicy_contextMenuRequested(QListViewItem*,const QPoint&amp;,int)</slot>
//     </connection>
//     <connection>
//         <sender>SOCKSBindAddressMany</sender>
//         <signal>contextMenuRequested(QListViewItem*,const QPoint&amp;,int)</signal>
//         <receiver>Running</receiver>
//         <slot>SOCKSBindAddressMany_contextMenuRequested(QListViewItem*,const QPoint&amp;,int)</slot>
//     </connection>
//     <connection>
//         <sender>PushAdd</sender>
//         <signal>clicked()</signal>
//         <receiver>Running</receiver>
//         <slot>PushAdd_clicked()</slot>
//     </connection>
//     <connection>
//         <sender>PushAdd2</sender>
//         <signal>clicked()</signal>
//         <receiver>Running</receiver>
//         <slot>PushAdd2_clicked()</slot>
//     </connection>
//     <connection>
//         <sender>kcfg_DefaultRunningNormalOptions</sender>
//         <signal>toggled(bool)</signal>
//         <receiver>Running</receiver>
//         <slot>kcfg_DefaultRunningNormalOptions_toggled(bool)</slot>
//     </connection>
//     <connection>
//         <sender>kcfg_HashedControlPassword</sender>
//         <signal>textChanged(const QString&amp;)</signal>
//         <receiver>Running</receiver>
//         <slot>kcfg_HashedControlPassword_textChanged(const QString&amp;)</slot>
//     </connection>
//     <connection>
//         <sender>kcfg_CookieAuthentication</sender>
//         <signal>toggled(bool)</signal>
//         <receiver>Running</receiver>
//         <slot>kcfg_CookieAuthentication_toggled(bool)</slot>
//     </connection>
//     <connection>
//         <sender>kcfg_ClientOnly</sender>
//         <signal>toggled(bool)</signal>
//         <receiver>Running</receiver>
//         <slot>kcfg_ClientOnly_toggled(bool)</slot>
//     </connection>
// </connections>
// <includes>
//     <include location="local" impldecl="in implementation">running.ui.h</include>
// </includes>
// <slots>
//     <slot>PushAdd_clicked()</slot>
//     <slot>PushAdd2_clicked()</slot>
//     <slot>SOCKSBindAddressMany_contextMenuRequested( QListViewItem *, const QPoint &amp; point, int )</slot>
//     <slot>slotDeleteEntry()</slot>
//     <slot>SOCKSPolicy_contextMenuRequested( QListViewItem *, const QPoint &amp; point, int )</slot>
//     <slot>slotDeletePolicyEntry()</slot>
//     <slot>kcfg_DefaultRunningNormalOptions_toggled( bool state )</slot>
//     <slot>kcfg_HashedControlPassword_textChanged( const QString &amp; )</slot>
//     <slot>kcfg_CookieAuthentication_toggled( bool )</slot>
//     <slot>kcfg_ClientOnly_toggled( bool state )</slot>
// </slots>
// <functions>
//     <function access="private" specifier="non virtual">init()</function>
// </functions>
// <layoutdefaults spacing="6" margin="11"/>
// <includehints>
//     <includehint>kurlrequester.h</includehint>
//     <includehint>kcombobox.h</includehint>
//     <includehint>kpushbutton.h</includehint>
// </includehints>




