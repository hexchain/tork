/***************************************************************************
 * $Id: trayicon.h,v 1.7 2008/07/31 19:56:28 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
/***************************************************************************
 *   Copyright (C) 2005 by                                                 *
 *   Joris Guisson <joris.guisson@gmail.com>                               *
 *   Ivan Vasic <ivasic@gmail.com>                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.             *
 ***************************************************************************/
#ifndef TRAYICON_H
#define TRAYICON_H


#include <ksystemtray.h>
#include <kpopupmenu.h>
#include <qpainter.h>
#include <kurl.h>
#include <tork.h>


class QString;
class TrayHoverPopup;
class tork;

/**
 * @author Joris Guisson
 * @author Ivan Vasic
*/
class TrayIcon : public KSystemTray
{
	Q_OBJECT
public:
	TrayIcon(tork *parent = 0, const char *name = 0);
	virtual ~TrayIcon();

	/// Update stats for system tray icon
	void updateStats(const QString &, const QString &,
                     const QString &,const QString &, const QStringList &, const QStringList &,
                     const QString &);
	
private:
	virtual void enterEvent(QEvent* ev);
	virtual void leaveEvent(QEvent* ev);
    virtual void dragEnterEvent(QDragEnterEvent *e);
    virtual void dropEvent (QDropEvent *o);
	void droppedfile (KURL::List url);
private:
	QPainter *paint;
	TrayHoverPopup* m_hover_popup;
	QPixmap m_kt_pix;
    tork *m_parent;

};


class SetMaxRate : public KPopupMenu
{
		Q_OBJECT
	public:
		SetMaxRate(tork *parent=0, const char *name=0); // type: 0 Upload; 1 Download
		~SetMaxRate()
		{}
		;

		void update();
	private:
		void makeMenu();
        tork *m_parent;
        int m_rate;
	private slots:
		void rateSelected(int id);
};

#endif
