/***************************************************************************
 * $Id: paranoia.ui.h,v 1.5 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

#include "torkconfig.h"
#include <klocale.h>
#include <qtooltip.h>
#include <qpopupmenu.h>
#include <kdebug.h>
#include <kiconloader.h>


void FilterTable::init()
{
    FilterRules->setSelectionMode( QListView::Extended );
    FilterRules->setColumnWidthMode(0, QListView::Manual);
    FilterRules->hideColumn(0);
    FilterRules->header()->setResizeEnabled(FALSE, 0);
    FilterRules->setResizeMode( QListView::NoColumn );

    SystemFilterRules->setSelectionMode( QListView::Extended );
    SystemFilterRules->setColumnWidthMode(0, QListView::Manual);
    SystemFilterRules->hideColumn(0);
    SystemFilterRules->header()->setResizeEnabled(FALSE, 0);
    SystemFilterRules->setResizeMode( QListView::NoColumn );

    QStringList filterRules = TorkConfig::filterRules();

    QListViewItem *tm;
	for ( QStringList::Iterator it = filterRules.begin(); it != filterRules.end(); ++it )
	{
		if ((*it).isEmpty())
			continue;
        QString active = (*it).section("%:%",0,0);
        QString desc = (*it).section("%:%",1,1);
        QString entry = (*it).section("%:%",2,2);
        
        tm = new QListViewItem(FilterRules,active,desc,entry);

        tm->setPixmap( 1, QPixmap(SmallIcon(active )));
    }


    QStringList systemFilterRules = TorkConfig::systemFilterRules();

	for ( QStringList::Iterator it = systemFilterRules.begin(); it != systemFilterRules.end(); ++it )
	{
		if ((*it).isEmpty())
			continue;
        QString active = (*it).section("%:%",0,0);
        QString desc = (*it).section("%:%",1,1);
        QString entry = (*it).section("%:%",2,2);
        
        tm = new QListViewItem(SystemFilterRules,active,desc,entry);

        tm->setPixmap( 1, QPixmap(SmallIcon(active )));
    }


}

void FilterTable::pushAdd_clicked()
{
    new QListViewItem(FilterRules,Description->text().replace("%:%",""),Filter->text().replace("%:%",""));

}

void FilterTable::pushAdd_Sys_clicked()
{
    new QListViewItem(SystemFilterRules,SystemDescription->text().replace("%:%",""),SystemFilter->text().replace("%:%",""));

}


void FilterTable::FilterRules_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{

    QPopupMenu *menu = new QPopupMenu( FilterRules );

    menu->clear();
    menu->insertItem( "Delete Entry", this,SLOT(slotDeleteEntry()) );
    menu->insertItem( "Toggle Entry", this,SLOT(slotToggleEntry()) );

    menu->popup( point );

}

void FilterTable::SystemFilterRules_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{

    QPopupMenu *menu = new QPopupMenu( SystemFilterRules );

    menu->clear();
    menu->insertItem( "Delete Entry", this,SLOT(slotDeleteSystemEntry()) );
    menu->insertItem( "Toggle Entry", this,SLOT(slotToggleSystemEntry()) );

    menu->popup( point );

}


void FilterTable::slotDeleteEntry( )
{

    QListViewItemIterator it(FilterRules, QListViewItemIterator::Selected);
    while ( it.current() ) {
        if (FilterRules->isSelected( it.current()))
            delete it.current();
        ++it;
    }


}

void FilterTable::slotToggleEntry( )
{

    QListViewItemIterator it(FilterRules, QListViewItemIterator::Selected);
    while ( it.current() ) {
        if (FilterRules->isSelected( it.current())){
            QString active = "button_cancel";
            if (it.current()->text(0).contains("button_cancel"))
                active = "button_ok";

            it.current()->setText(0,active);
            it.current()->setPixmap( 1, QPixmap(SmallIcon(active)));
        }
        ++it;
    }


}

void FilterTable::slotDeleteSystemEntry( )
{

    QListViewItemIterator it(SystemFilterRules, QListViewItemIterator::Selected);
    while ( it.current() ) {
        if (SystemFilterRules->isSelected( it.current()))
            delete it.current();
        ++it;
    }


}

void FilterTable::slotToggleSystemEntry( )
{

    QListViewItemIterator it(SystemFilterRules, QListViewItemIterator::Selected);
    while ( it.current() ) {
        if (SystemFilterRules->isSelected( it.current())){
            QString active = "button_cancel";
            if (it.current()->text(0).contains("button_cancel"))
                active = "button_ok";

            it.current()->setText(0,active);
            it.current()->setPixmap( 1, QPixmap(SmallIcon(active)));
        }
        ++it;
    }


}

// <connections>
//     <connection>
//         <sender>pushAdd</sender>
//         <signal>clicked()</signal>
//         <receiver>FilterTable</receiver>
//         <slot>pushAdd_clicked()</slot>
//     </connection>
//     <connection>
//         <sender>FilterRules</sender>
//         <signal>contextMenuRequested(QListViewItem*,const QPoint&amp;,int)</signal>
//         <receiver>FilterTable</receiver>
//         <slot>FilterRules_contextMenuRequested(QListViewItem*,const QPoint&amp;,int)</slot>
//     </connection>
// </connections>
// <includes>
//     <include location="local" impldecl="in implementation">firewallsproxies.ui.h</include>
// </includes>
// <slots>
//     <slot>pushAdd_clicked()</slot>
//     <slot>FilterRules_contextMenuRequested( QListViewItem *, const QPoint &amp; point, int )</slot>
//     <slot>slotDeleteEntry()</slot>
// </slots>
// <functions>
//     <function access="private" specifier="non virtual">init()</function>
// </functions>
// <layoutdefaults spacing="6" margin="11"/>
// </UI>
