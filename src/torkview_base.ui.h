/***************************************************************************
 * $Id: torkview_base.ui.h,v 1.48 2009/06/20 09:29:15 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

#include <qtooltip.h>
#include <qpopupmenu.h>
#include <kdebug.h>
#include <qclipboard.h>

#include <kapplication.h>
#include <kiconloader.h>
#include <kstandarddirs.h>
#include "torkconfig.h"
#include "tork.h"
#include "crypto.h"

#ifndef EXTERNAL_GEOIP
# include "GeoIP-1.4.0/libGeoIP/GeoIP.h"
#else
# include <GeoIP.h>
#endif

bool m_LeaveStreamsUnattached;

typedef QMap<QString, QListViewItem*> ResolveMap;
ResolveMap resolveMap;
typedef QMap<QListViewItem*, int > ResolveColMap;
ResolveColMap resolveColMap;

void torkview_base::init()
{

    serverFilter->setListView((KListView *)serverList);
    QValueList<int> tmp;
    tmp << 1;
    serverFilter->setSearchColumns(tmp);

    clearButton->setIconSet(SmallIconSet("locationbar_erase"));
    connect(clearButton, SIGNAL( clicked() ),
                    serverFilter, SLOT(clear()) );

    serverList->addColumn( kapp->iconLoader()->loadIconSet("tork_torsmall", 
                           KIcon::Small),"", 24 );
    serverList->addColumn( "Server" );
    serverList->addColumn( "FP" );
    serverList->addColumn( "conditions" );
    serverList->addColumn( "countrycode" );
    serverList->addColumn( "ip" );

    serverList->setSelectionMode( QListView::Extended );
    serverList->setColumnWidthMode(2, QListView::Manual);
    serverList->hideColumn(2);
    serverList->setColumnWidthMode(3, QListView::Manual);
    serverList->hideColumn(3);
    serverList->setColumnWidthMode(4, QListView::Manual);
    serverList->hideColumn(4);
    serverList->setColumnWidthMode(5, QListView::Manual);
    serverList->hideColumn(5);
    serverList->header()->setResizeEnabled(FALSE, 1);
    serverList->setResizeMode( QListView::NoColumn );
    serverList->setHScrollBarMode(QScrollView::AlwaysOff);

    streamList->addColumn( "ID" );
    streamList->addColumn(  tr2i18n("Source")  );
    streamList->addColumn(  tr2i18n("Host/Port")  );
    streamList->addColumn(  kapp->iconLoader()->loadIconSet("tork_torsmall", KIcon::Small),"", 24  );
    streamList->addColumn(  tr2i18n("Speed")  );
    streamList->addColumn(  tr2i18n("Exit")  );
    streamList->addColumn(  tr2i18n("Circuit")  );

    streamList->setSelectionMode( QListView::Extended );
    streamList->setColumnWidthMode(0, QListView::Manual);
    streamList->hideColumn(0);
    streamList->header()->setResizeEnabled(FALSE, 0);
    streamList->setResizeMode( QListView::NoColumn );


    circuitList->addColumn( tr2i18n( "ID" ) );
    circuitList->addColumn( kapp->iconLoader()->loadIconSet("tork_torsmall", KIcon::Small),"", 24 );
    circuitList->addColumn( tr2i18n( "Path" ) );
    circuitList->setSelectionMode( QListView::Extended );
    circuitList->setColumnWidthMode(0, QListView::Manual);
    circuitList->hideColumn(0);
    circuitList->header()->setResizeEnabled(FALSE, 0);
    circuitList->setResizeMode( QListView::LastColumn );


    ORList->addColumn( kapp->iconLoader()->loadIconSet("tork_torsmall", KIcon::Small),"", 24 );
    ORList->addColumn( tr2i18n( "Server" ) );
    ORList->setSelectionMode( QListView::Extended );
    ORList->setColumnWidthMode(0, QListView::Manual);
    ORList->setResizeMode( QListView::LastColumn );

    TorTraffic->setColumnWidthMode(0, QListView::Manual);
    TorTraffic->hideColumn(0);
    TorTraffic->header()->setResizeEnabled(FALSE, 0);
    TorTraffic->setResizeMode( QListView::NoColumn );
    TorTraffic->setSorting ( 1, true);
    NonTorTraffic->setSorting ( 0, true);

    m_LeaveStreamsUnattached = false;

    infoList->setColumnWidthMode(1, QListView::Manual);
    infoList->hideColumn(1);
    infoList->header()->setResizeEnabled(FALSE, 1);
    infoList->setSorting ( 0, true);

    kcfg_LogNonTorTraffic->setChecked(TorkConfig::logNonTorTraffic());
    kcfg_LogTorTraffic->setChecked(TorkConfig::logTorTraffic());
    kcfg_LogNonTorTraffic_toggled(TorkConfig::logNonTorTraffic());
    kcfg_LogTorTraffic_toggled(TorkConfig::logTorTraffic());

    NonTorTrafficMaxEntries->setValue(TorkConfig::nonTorTrafficMaxEntries());

}

void torkview_base::stopDisplayingBubble(QListViewItem*,const QPoint&,int)
{
    TorkConfig::setDisplayBubble(false);
}

void torkview_base::streamList_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{

    QPopupMenu *menu = new QPopupMenu( streamList );

    menu->clear();

    if (streamList->childCount() > 0)
        menu->insertItem( "Close Connection", this,SLOT(slotCloseStream()) );
    if (!m_LeaveStreamsUnattached)
        menu->insertItem( "Let Me Drag Connections To Circuits Myself", this,SLOT(slotAttachStreams()) );
    else
        menu->insertItem( "Attach Connections To Circuits Automatically", this,SLOT(slotAttachStreams()) );

    menu->popup( point );

}


void torkview_base::slotAttachStreams( )
{

    m_LeaveStreamsUnattached = !m_LeaveStreamsUnattached;
    emit attachStreams(m_LeaveStreamsUnattached);
}
void torkview_base::slotCloseStream( )
{
    kdDebug() << "deleting entry" << endl;

    QListViewItemIterator it(streamList, QListViewItemIterator::Selected);
    while ( it.current() ) {
        if (streamList->isSelected( it.current()))
            emit closeStream(it.current()->text(0));
        ++it;
    }


}

void torkview_base::circuitList_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{

    if (circuitList->childCount() == 0)
        return;

    QPopupMenu *menu = new QPopupMenu( circuitList );

    menu->clear();
    menu->insertItem( "Close Circuit", this,SLOT(slotCloseCircuit()) );
    menu->popup( point );

}


void torkview_base::slotCloseCircuit( )
{
    kdDebug() << "deleting entry" << endl;

    QListViewItemIterator it(circuitList, QListViewItemIterator::Selected);
    while ( it.current() ) {
        if (circuitList->isSelected( it.current()))
            emit closeCircuit(it.current()->text(0));
        ++it;
    }


}

void torkview_base::serverList_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{

    if (serverList->childCount() == 0)
        return;

    QPopupMenu *menu = new QPopupMenu( serverList );
    QPopupMenu *m_ThisSession =  new KPopupMenu( this );
    QPopupMenu *m_ThisPerm =  new KPopupMenu( this );

    menu->clear();
    menu->insertItem( i18n("For This Session Only"), m_ThisSession );
    menu->insertItem( i18n("From Now On"), m_ThisPerm );
    menu->insertItem( "Add to My Family", this, SLOT(slotAddToFamily()) );

    m_ThisSession->insertItem( i18n("Always Use Server As An Exit"), this,
         SLOT(slotAddSessionStrictExitNodes()) );
    m_ThisSession->insertItem( i18n("Try To Use Server As an Exit"), this, SLOT(slotAddSessionExitNodes()) );
    m_ThisSession->insertItem( i18n("Never Use Server At All"), this, SLOT(slotAddSessionExcludeNodes()) );
    m_ThisSession->insertItem( i18n("Never Use Country At All"), this, SLOT(slotAddSessionExcludeCountry()) );

    m_ThisPerm->insertItem( i18n("Always Use Server As An Exit"), this, SLOT(slotAddPermStrictExitNodes()) );
    m_ThisPerm->insertItem( i18n("Try To Use Server As an Exit"), this, SLOT(slotAddPermExitNodes()) );
    m_ThisPerm->insertItem( i18n("Never Use Server At All"), this,  SLOT(slotAddPermExcludeNodes()) );
    m_ThisPerm->insertItem( i18n("Never Use Country At All"), this,  SLOT(slotAddPermExcludeCountry()) );

    menu->insertSeparator();
    menu->insertItem( "Reset Session Settings", this, SLOT(slotClearNodes()) );
    menu->popup( point );

}

void torkview_base::slotClearNodes( )
{
    emit clearNodes();
}

void torkview_base::slotAddToFamily( )
{
    QStringList currentList;
    currentList = TorkConfig::myFamily();

    QListViewItemIterator it(serverList, QListViewItemIterator::Selected | QListViewItemIterator::Visible);
    while ( it.current() ) {
            if (!TorkConfig::myFamily().contains(QString("%1-%2-%3")
                .arg((*it)->text(4)).arg((*it)->text(2)).arg((*it)->text(1)))){
                    QStringList existingServers = TorkConfig::myFamily();
                    existingServers.append(QString("%1-%2-%3")
                        .arg((*it)->text(4)).arg((*it)->text(2)).arg((*it)->text(1)));
                    TorkConfig::setMyFamily(existingServers);
            }
        ++it;
    }
    TorkConfig::writeConfig();
}

void torkview_base::slotAddPermExcludeCountry( )
{
    QStringList selectedCountries = selectByCountry();

    QStringList newCountries = TorkConfig::excludeCountries();
    for ( QStringList::Iterator it = selectedCountries.begin(); it != selectedCountries.end(); ++it )
	{
        if ((!TorkConfig::excludeCountries().contains(*it)) &&
           (!newCountries.contains(*it)))
            newCountries.append(*it);
    }
    TorkConfig::setExcludeCountries(newCountries);

    addExcludeNodes(true,TorkConfig::excludeCountries());
}

void torkview_base::slotAddPermExcludeNodes( )
{
    addExcludeNodes(true, QStringList());
}

QStringList torkview_base::selectByCountry( )
{

    QStringList selectCountries;

    QListViewItemIterator it(serverList, QListViewItemIterator::Selected | QListViewItemIterator::Visible);
    while ( it.current() ) {
        QString cc = (*it)->text(4);
        selectCountries.append(cc);
        ++it;
    }
    return selectCountries;
}

void torkview_base::slotAddSessionExcludeCountry( )
{

    addExcludeNodes(false, selectByCountry());
}

void torkview_base::slotAddSessionExcludeNodes( )
{
    addExcludeNodes(false, QStringList());
}

void torkview_base::slotAddPermExitNodes( )
{
    addExitNodes(true, false);
}

void torkview_base::slotAddPermStrictExitNodes( )
{
    addExitNodes(true, true);
}

void torkview_base::slotAddSessionExitNodes( )
{
    addExitNodes(false, false);
}

void torkview_base::slotAddSessionStrictExitNodes( )
{
    addExitNodes(false, true);
}

void torkview_base::slotAddPermEntryNodes( )
{
    addEntryNodes(true);
}

void torkview_base::slotAddSessionEntryNodes( )
{
    addEntryNodes(false);
}

void torkview_base::addExcludeNodes(bool perm, QStringList ccs )
{

    QStringList currentList;
    currentList = TorkConfig::currentExcludeNodes();
    QStringList permList;
    permList = TorkConfig::excludeNodes();
    QListViewItemIterator::IteratorFlag filter;

    if (ccs.isEmpty())
        filter = QListViewItemIterator::IteratorFlag(QListViewItemIterator::Selected | QListViewItemIterator::Visible);
    else
        filter = QListViewItemIterator::IteratorFlag();

    QListViewItemIterator it(serverList, filter);

    while ( it.current() ) {
        if (!ccs.isEmpty()){
            if (!ccs.contains((*it)->text(4))){
                ++it;
                continue;
            }
        }

        QString node = "$"+getFPFromFPDigest((*it)->text(2));
        currentList.append(node);
        if (perm){
            permList.append(node);
            if (!TorkConfig::excludeServersHistory().contains(QString("%1-%2-%3")
                .arg((*it)->text(4)).arg((*it)->text(2)).arg((*it)->text(1)))){
                QStringList existingServers = TorkConfig::excludeServersHistory();
                existingServers.append(QString("%1-%2-%3")
                    .arg((*it)->text(4)).arg((*it)->text(2)).arg((*it)->text(1)));
                TorkConfig::setExcludeServersHistory(existingServers);
            }

        }
        //kdDebug() << getFPFromFPDigest((*it)->text(2)) << endl;
        ++it;
    }
    TorkConfig::setCurrentExcludeNodes(currentList);
    TorkConfig::setExcludeNodes(permList);

    QListView* tmp = dynamic_cast<QListView*>(circuitList);
    emit closeAllCircuits(tmp);
    emit updateExcludeNodes();

    emit copyOldConfig();

}

void torkview_base::addEntryNodes( bool perm )
{

    QStringList currentList;
    currentList = TorkConfig::currentEntryNodes();
    QStringList permList;
    permList = TorkConfig::entryNodes();

    QListViewItemIterator it(serverList, QListViewItemIterator::Selected | QListViewItemIterator::Visible);
    while ( it.current() ) {
        QString node = "$"+getFPFromFPDigest((*it)->text(2));
        currentList.append(node);
        if (perm)
            permList.append(node);
        //kdDebug() << getFPFromFPDigest((*it)->text(2)) << endl;
        ++it;
    }
    TorkConfig::setCurrentEntryNodes(currentList);
    TorkConfig::setEntryNodes(permList);
    emit copyOldConfig();
    QListView* tmp = dynamic_cast<QListView*>(circuitList);
    emit closeAllCircuits(tmp);
    emit updateEntryNodes();

}

void torkview_base::addExitNodes(bool perm , bool strict)
{

    QStringList currentList;
    currentList = TorkConfig::currentExitNodes();
    QStringList permList;
    permList = TorkConfig::exitNodes();

    QListViewItemIterator it(serverList, QListViewItemIterator::Selected | QListViewItemIterator::Visible);
    while ( it.current() ) {
        QString node = "$"+getFPFromFPDigest((*it)->text(2));
        currentList.append(node);
        if (perm){
            permList.append(node);
            if (!TorkConfig::includeServersHistory().contains(QString("%1-%2-%3")
                .arg((*it)->text(4)).arg((*it)->text(2)).arg((*it)->text(1)))){
                QStringList existingServers = TorkConfig::includeServersHistory();
                existingServers.append(QString("%1-%2-%3")
                    .arg((*it)->text(4)).arg((*it)->text(2)).arg((*it)->text(1)));
                TorkConfig::setIncludeServersHistory(existingServers);
            }

        }
        //kdDebug() << getFPFromFPDigest((*it)->text(2)) << endl;
        ++it;
    }
    TorkConfig::setCurrentExitNodes(currentList);
    TorkConfig::setExitNodes(permList);
    if (perm)
      TorkConfig::setStrictExitNodes(strict);

    QListView* tmp = dynamic_cast<QListView*>(circuitList);
    emit closeAllCircuits(tmp);

    emit updateExitNodes();
    emit updateStrictExitNodes(strict);
    emit copyOldConfig();

}
void torkview_base::clearInfo_clicked()
{
    infoList->clear();
}


void torkview_base::clearTorTraffic_clicked()
{
    TorTraffic->clear();
}


void torkview_base::clearNonTorTraffic_clicked()
{

    NonTorTraffic->clear();

}

void torkview_base::NonTorTrafficMaxEntries_valueChanged( int newval)
{
    TorkConfig::setNonTorTrafficMaxEntries(newval);
    TorkConfig::writeConfig();
}


void torkview_base::useNewIdentity_clicked( int, int )
{

    emit newIdentity();
}


void torkview_base::useKonqWithTor_clicked( int, int )
{

    emit konqWithTor();
}


void torkview_base::viewNetworkList_clicked( int, int )
{

    emit networkList();
}


void torkview_base::viewHiddenServices_clicked( int, int )
{

    emit hiddenServices();
}


void torkview_base::kcfg_LogNonTorTraffic_toggled( bool state)
{
    TorkConfig::setLogNonTorTraffic(state);
    NonTorTraffic->setEnabled(state);
    emit toggleNonTorTraffic( state );
}


void torkview_base::kcfg_LogTorTraffic_toggled( bool state)
{
    TorkConfig::setLogTorTraffic(state);
/*    TorkConfig::writeConfig();*/
    TorTraffic->setEnabled(state);
    emit toggleTorTraffic( state );
}


void torkview_base::ShowHostNames_toggled( bool safe)
{
  emit safeLogging(!safe);
}

void torkview_base::copyLogLines()
{
  QClipboard *cb = QApplication::clipboard();
  QString logLines;

  QListViewItemIterator it(infoList, QListViewItemIterator::Selected | QListViewItemIterator::Visible);
  while ( it.current() ) {
    // Copy text into the clipboard
    logLines.append((*it)->text(0));
    logLines.append("\t");
    logLines.append((*it)->text(2));
    logLines.append("\t");
    logLines.append((*it)->text(3));
    logLines.append("\n");

     ++it;
  }

  cb->setText( logLines, QClipboard::Clipboard );

}

void torkview_base::selectAllLogLines()
{
  QListViewItemIterator it(infoList);
  while ( it.current() ) {
     (*it)->setSelected(true);
     ++it;
  }
  infoList->repaint();
}

void torkview_base::infoList_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{
    QPopupMenu *menu = new QPopupMenu( infoList );

    menu->clear();
    menu->insertItem( SmallIconSet("copy"), "Copy", this, SLOT(copyLogLines()) );
    menu->insertSeparator();
    menu->insertItem( "Select All", this, SLOT(selectAllLogLines()) );
    menu->popup( point );

}


void torkview_base::TorTraffic_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{

    QPopupMenu *menu = new QPopupMenu( TorTraffic );

    menu->clear();
    menu->insertItem( SmallIconSet("help"), "Resolve Hostname/Address", this, SLOT(queryHostName()) );
    menu->popup( point );

}

void torkview_base::queryHostName()
{
    QRegExp rx("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
    QRegExp rx2("[^:]+");

    QListView *tmpList;
    QString addressToResolve;
    QString addressToMatch;

    if (TorTraffic->hasFocus()) {
      tmpList = TorTraffic;
    }else{
      tmpList = NonTorTraffic;
    }

    int col = (tmpList == TorTraffic) ? 2 : 1;
    rx.search(tmpList->selectedItem()->text(col));
    QString tmp = rx.cap(0);
    rx2.search(tmpList->selectedItem()->text(col));
    QString tmp2 = rx2.cap(0);

    if (!tmp.isEmpty()){
        addressToMatch = QString("REVERSE[%1]").arg(tmp);
        addressToResolve = tmp;
    }else{
        addressToResolve = tmp2;
        addressToMatch = tmp2;
    }

    resolveMap[addressToMatch] = tmpList->selectedItem();
    resolveColMap[tmpList->selectedItem()] = col;

    emit resolveAddress(addressToResolve);
}

void torkview_base::resolvedAddress(const QString &info)
{
    QString type = info.section(" ",0,0);
    QString address = info.section(" ",1,1);

    if (resolveMap[type]){
        int column = resolveColMap[resolveMap[type]];
        resolveMap[type]->setText(column,address);
        resolveMap.erase(type);
        resolveColMap.erase(resolveMap[type]);
    }
}

void torkview_base::NonTorTraffic_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{
    QPopupMenu *menu = new QPopupMenu( NonTorTraffic );

    menu->clear();
    menu->insertItem( SmallIconSet("help"), "Resolve Hostname/Address", this, SLOT(queryHostName()) );
    menu->popup( point );

}
