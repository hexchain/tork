/***************************************************************************
** $Id: configdialog.cpp,v 1.51 2008/07/31 19:56:25 hoganrobert Exp $
*   Copyright (C) 2006 - 2008 Robert Hogan                                *
*   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
//Adapted from configdialog.cpp in amaroK:
/***************************************************************************
begin                : 2004/02/07
copyright            : (C) Mark Kretschmann
email                : markey@web.de
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "configdialog.h"
#include "konqueror.h"
#include "firewallsproxies.h"
#include "paranoia.h"
#include "maxmin.h"
#include "running.h"
#include "torservers.h"
#include "usability.h"
#include "torkconfig.h"
#include "server.h"
#include "quickconfig.h"
#include "hiddensrvs.h"
#include "portsandnames.h"
#include "tork.h"
#include "crypto.h"
#include "../config.h"

#include <qcombobox.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qlineedit.h>
#include <qmessagebox.h>
#include <qobjectlist.h>
#include <qpushbutton.h>
#include <qradiobutton.h>
#include <qspinbox.h>
#include <qtextcodec.h>
#include <qtooltip.h>
#include <qvbox.h>
#include <qlistview.h>
#include <qcheckbox.h>

#include <kapplication.h> //kapp
#include <kcombobox.h>
#include <kiconloader.h>
#include <klineedit.h>
#include <klocale.h>
#include <kstandarddirs.h>
#include <kdebug.h>
#include <dcopref.h>
#include <kconfig.h>
#include <kmessagebox.h>

//////////////////////////////////////////////////////////////////////////////////////////
// PUBLIC
//////////////////////////////////////////////////////////////////////////////////////////

TorkConfigDialog::TorkConfigDialog( QWidget *parent, const char* name, KConfigSkeleton *config )
        : KConfigDialog( parent, name, config ),m_parent(parent)
{
    setWFlags( WDestructiveClose );

    // IMPORTANT Don't simply change the page names, they are used as identifiers in other parts of the app.
        m_firewallsproxies = new FirewallsProxies( 0, "Firewall/Censor Evasion" );
        m_usability = new Usability( 0, "Usability" );
        m_torservers = new TorServers( 0, "My Network View" );
	    m_maxmin = new MaxMin( 0, "My Server Bandwidth" );
	    m_running = new Running( 0, "My Tor Client" );
        m_myserver = new MyServer( this, "My Tor Server" );
        m_myhidden = new MyHidden( 0, "My Hidden" );
        m_quickconfig = new QuickConfig( 0, "Quick Configure" );
        m_konq = new Konq( 0, "Konqueror" );
        m_filtertable = new FilterTable( 0, "FailSafe" );

    // add pages
    addPage( m_quickconfig, i18n( "Quick Configure" ), "cache", i18n( "Quick Configuration" ) );
#ifndef LIVECD
    addPage( m_konq, i18n( "Konqueror" ), "tork_konqueror_play", i18n( "Konqueror and Privacy Proxy" ) );
#endif

#ifndef LIVECD
    addPage( m_running, i18n( "My Tor Client" ), "launch", i18n( "Configure My Client" ) );
#endif
    addPage( m_firewallsproxies, i18n( "Firewall/Censor Evasion" ), "proxy", i18n( "Configure Firewalls Proxies" ) );

    if (TorkConfig::advancedMode()){
#ifndef LIVECD
#ifdef USE_FAILSAFE
      addPage( m_filtertable, i18n( "FailSafe" ), "gv", i18n( "Configure FailSafe Settings" ) );
#endif
#endif
      addPage( m_usability,   i18n( "Usability" ), "idea", i18n( "Configure Usability" ) );
    }
    if ((!TorkConfig::excludeNodes().isEmpty()) ||
        (!TorkConfig::exitNodes().isEmpty())){
        addPage( m_torservers, i18n( "My Network View" ), "network", 
                 i18n( "Configure My Network View" ) );
    }
    if (!TorkConfig::clientOnly()){
        addPage( m_myserver, i18n( "My Tor Server" ), "system", i18n( "Configure My Server" ) );
        addPage( m_maxmin, i18n( "My Server Bandwidth" ), "filter", i18n( "Configure My Bandwidth" ) );
    }
    if (!TorkConfig::hiddenServices().isEmpty())
        addPage( m_myhidden, i18n( "My Hidden Services" ), "tork_agent", 
                         i18n( "Configure My Hidden Services" ) );
    

    QObjectList *list = queryList( "QLabel", "infoPixmap" );
    for( QObject *label = list->first(); label; label = list->next() )
        static_cast<QLabel*>(label)->setPixmap( QMessageBox::standardIcon( QMessageBox::Information ) );
    delete list;

    //stop KFont Requesters getting stupidly large
    list = queryList( "QLabel", "m_sampleLabel" );
    for( QObject *label = list->first(); label; label = list->next() )
        static_cast<QLabel*>(label)->setMaximumWidth( 250 );
    delete list;

    connect( m_usability->LongLivedPorts, SIGNAL( selectionChanged() ), this, SLOT( enableApply() ) );
    connect( m_torservers->ExitNodes, SIGNAL( selectionChanged() ), this, SLOT( enableApply() ) );
    connect( m_torservers->ExcludeNodes, SIGNAL( selectionChanged() ), this, SLOT( enableApply() ) );
    connect( m_torservers->MapAddress, SIGNAL( selectionChanged() ), this, SLOT( enableApply() ) );
    connect( m_torservers->AllowUnverifiedNodes, SIGNAL( selectionChanged() ), this, SLOT( enableApply() ) );
    connect( m_firewallsproxies->ReachableAddresses, SIGNAL( selectionChanged() ), this, SLOT( enableApply() ) );
    connect( m_filtertable->FilterRules, SIGNAL( selectionChanged() ), this, SLOT( enableApply() ) );
    connect( m_running->SOCKSPolicy, SIGNAL( selectionChanged() ), this, SLOT( enableApply() ) );
    connect( m_running->SOCKSBindAddressMany, SIGNAL( selectionChanged() ), this, SLOT( enableApply() ) );
    connect( m_running->PushAdd, SIGNAL( clicked() ), this, SLOT( enableApply() ) );
    connect( m_running->PushAdd2, SIGNAL( clicked() ), this, SLOT( enableApply() ) );
    connect( m_myserver->kcfg_ClientOnly, SIGNAL( toggled(bool) ), this, SLOT( showServerOpts(bool) ) );
    connect( m_usability->TrackHostExits, SIGNAL( selectionChanged() ), this, SLOT( enableApply() ) );
    connect( m_usability->PushAdd, SIGNAL( clicked() ), this, SLOT( enableApply() ) );

    connect( m_quickconfig, SIGNAL( configChanged(const int &) ), this, SLOT( showHidePages(const int &) ) );
    //connect( m_usability, SIGNAL(settingsChanged()), SLOT(updateButtons()) ); 

    // Always apply settings when OK or Apply is pressed.
    //connect( this, SIGNAL(settingsChanged()), this,
    //               SLOT(updateCustomSettings()) );
    connect( this, SIGNAL(okClicked()), this,
                   SLOT(updateCustomSettings()) );
    connect( this, SIGNAL(applyClicked()), this,
                   SLOT(updateCustomSettings()) );
    connect( this, SIGNAL(updateServerButton( )), parent,
                    SLOT(updateServerButton( )) );

    connect( m_firewallsproxies, SIGNAL(uncensorSelected( bool )), parent,
                                 SLOT(toggleServerButton( bool )) );

    if (TorkConfig::currentTorVersion().left(3) == "0.1"){
        m_filtertable->setEnabled(false);
    }
    showHidePages(m_quickconfig->QuickConfigure->currentItem());

}

void TorkConfigDialog::showHidePages( const int &type )
{
    switch (type) {
            case 0 : //Tor client and server with default settings
            case 1 : //Tor client and relay server with default settings
            case 2 : //Tor client and relay server with default settings
            case 3 : //Tor client and relay server with default settings
            case 4 : //Tor client and relay server with default settings
               m_running->setEnabled(false); 
               m_usability->setEnabled(false); 
               m_torservers->setEnabled(false); 
               m_maxmin->setEnabled(false); 
               m_firewallsproxies->setEnabled(true); 
               m_myserver->setEnabled(false); 
               break;
            case 5 : //Use custom settings
            case 6 : //Use custom settings
            case 7 : //Use custom settings
               m_running->setEnabled(true); 
               m_usability->setEnabled(true); 
               m_torservers->setEnabled(true); 
               m_maxmin->setEnabled(true); 
               m_firewallsproxies->setEnabled(true); 
               m_myserver->ServerInfo->setEnabled(!TorkConfig::clientOnly());
               break;

    }

    show();

}

TorkConfigDialog::~TorkConfigDialog()
{
    emit updateServerButton( );
}

void TorkConfigDialog::showServerOpts( bool state)
{
    m_myserver->ServerInfo->setEnabled(!state);
}


/** Show page by object name */
void TorkConfigDialog::showPage( const QCString& page )
{
    for( uint index = 0; index < m_pageList.count(); index++ ) {
        if ( m_pageList[index]->name() == page ) {
            KConfigDialog::showPage( index );
            return;
        }
    }
}

/** Reimplemented from KConfigDialog */
void TorkConfigDialog::addPage( QWidget *page, const QString &itemName, const QString &pixmapName, const QString &header, bool manage )
{
    // Add the widget pointer to our list, for later reference
    m_pageList << page;

    KConfigDialog::addPage( page, itemName, pixmapName, header, manage );
}


void TorkConfigDialog::enableApply()
{
    enableButtonApply(true);
}

void TorkConfigDialog::updateCustomSettings()
{

    kdDebug() << "applied config" << endl;

    //When settings are changed in the config dialog, this is called
    //to update all possibly changed values and apply them to Tor.

    //QListViews need to be handled outside of kconfigxt
    TorkConfig::setQuickConfigure( m_quickconfig->QuickConfigure->currentItem() );

    TorkConfig::setAccountingStart(m_myserver->AccountingStart->currentText());
//     TorkConfig::setStatusFetchPeriodUnit(m_torservers->StatusFetchPeriodUnit->currentText());
//     TorkConfig::setDirFetchPeriodUnit(m_torservers->DirFetchPeriodUnit->currentText());

    //Unverified Servers
    if ( m_torservers->AllowUnverifiedNodes->childCount() > 0 ) {
        QStringList v_allowUnverifiedNodes;
        QListViewItemIterator it( m_torservers->AllowUnverifiedNodes, QListViewItemIterator::Selected );
        while ( it.current() ) {
            v_allowUnverifiedNodes.append(it.current()->text(0));
	        ++it;
        }
        TorkConfig::setAllowUnverifiedNodes( v_allowUnverifiedNodes );
    }

    //Friendly Servers
    if ( m_torservers->MapAddress->childCount() > 0 ) {
        QStringList v_mapAddress;
        QListViewItemIterator it( m_torservers->MapAddress );
        while ( it.current() ) {
            QString s_mapAddress = it.current()->text(0) + " " + it.current()->text(0) + "." +
                it.current()->text(1) + "." + it.current()->text(2);
            v_mapAddress.append(s_mapAddress);
	        ++it;
        }
        TorkConfig::setMapAddress( v_mapAddress );
    }else
        TorkConfig::setMapAddress( "" );

    //Exclude Countries
    QStringList cclist; 
    for (unsigned int index = 0; index != m_torservers->CountryList->count(); ++index){
        cclist.append(m_torservers->ccode(m_torservers->CountryList->item(index)->text()));
    }
    TorkConfig::setExcludeCountries(cclist);

//     Preferred Countries
//     cclist.clear();
//     for (unsigned int index = 0; index != m_torservers->PreferredCountryList->count(); ++index){
//         cclist.append(m_torservers->ccode(m_torservers->PreferredCountryList->item(index)->text()));
//     }
//     TorkConfig::setPreferredCountries(cclist);

    //Enemy Servers
    if ( m_torservers->ExcludeNodes->childCount() > 0 ) {
        QStringList v_excludeNodes;
        QStringList v_excludeServersHistory;
        QListViewItemIterator it( m_torservers->ExcludeNodes );
        while ( it.current() ) {
            v_excludeNodes.append("$"+getFPFromFPDigest(it.current()->text(2)));
            v_excludeServersHistory.append(it.current()->text(1) + "-" + 
                                           it.current()->text(2) + "-" + it.current()->text(3));
	        ++it;
        }
        TorkConfig::setExcludeNodes( v_excludeNodes );
        TorkConfig::setExcludeServersHistory( v_excludeServersHistory );
    }else{
        TorkConfig::setExcludeNodes( QStringList() );
        TorkConfig::setExcludeServersHistory( QStringList() );
    }

    //Exit Servers
    if ( m_torservers->ExitNodes->childCount() > 0 ) {
        QStringList v_exitNodes;
        QStringList v_includeServersHistory;
        QListViewItemIterator it( m_torservers->ExitNodes );
        while ( it.current() ) {
            v_exitNodes.append("$"+getFPFromFPDigest(it.current()->text(2)));
            v_includeServersHistory.append(it.current()->text(1) + "-" + 
                                           it.current()->text(2) + "-" + it.current()->text(3));

	        ++it;
        }
        TorkConfig::setExitNodes( v_exitNodes );
        TorkConfig::setIncludeServersHistory( v_includeServersHistory );
    }else{
        TorkConfig::setExitNodes( QStringList() );
        TorkConfig::setIncludeServersHistory( QStringList() );
    }

    //Family Servers
    if ( m_myserver->MyFamily->childCount() > 0 ) {
        QStringList v_myFamily;
        QListViewItemIterator it( m_myserver->MyFamily, QListViewItemIterator::Selected );
        while ( it.current() ) {
            v_myFamily.append(it.current()->text(1) + "-" + 
                              it.current()->text(2) + "-" + it.current()->text(3));
	        ++it;
        }
        TorkConfig::setMyFamily( v_myFamily );
    }else{
        TorkConfig::setMyFamily( QStringList() );
    }

    //Track Host Exits
    if ( m_usability->TrackHostExits->childCount() > 0 ) {
        QStringList v_trackHostExits;
        QListViewItemIterator it( m_usability->TrackHostExits );
        while ( it.current() ) {
            v_trackHostExits.append(it.current()->text(0));
	        ++it;
        }
        TorkConfig::setTrackHostExits( v_trackHostExits );
    }else
        TorkConfig::setTrackHostExits( "" );

    //Long Lived Ports
    if ( m_usability->LongLivedPorts->childCount() > 0 ) {
        QStringList v_longLivedPorts;
        QListViewItemIterator it( m_usability->LongLivedPorts, QListViewItemIterator::Selected );
        while ( it.current() ) {
            int i;
            for (i = 0; _port_name[i].port; ++i) {
                portsandnames_t *pics = &_port_name[i];
                if (it.current()->text(0) == pics->name){
                    v_longLivedPorts.append(pics->port);
                }
            }

	        ++it;
        }
        TorkConfig::setLongLivedPorts( v_longLivedPorts );
    }

    //Socks Bind Addresses
    if ( m_running->SOCKSBindAddressMany->childCount() > 0 ) {
        QStringList v_sOCKSBindAddressMany;
        QListViewItemIterator it( m_running->SOCKSBindAddressMany );
        while ( it.current() ) {
            QString s_sOCKSBindAddressMany = it.current()->text(0) + ":" + it.current()->text(1);
            v_sOCKSBindAddressMany.append(s_sOCKSBindAddressMany);
	        ++it;
        }
        TorkConfig::setSOCKSBindAddressMany( v_sOCKSBindAddressMany );
    }else
        TorkConfig::setSOCKSBindAddressMany( "" );

    //Exit Policy
    if ( m_myserver->ExitPolicy->childCount() > 0 ) {
        QStringList v_exitPolicy;
        QListViewItemIterator it( m_myserver->ExitPolicy );
        while ( it.current() ) {
            QString s_exitPolicy = it.current()->text(0) + " " + it.current()->text(1) + ":" + it.current()->text(2);
            v_exitPolicy.append(s_exitPolicy);
	        ++it;
        }
        TorkConfig::setExitPolicy( v_exitPolicy );
    }else
        TorkConfig::setExitPolicy( "" );

    //Socks Policy
    if ( m_running->SOCKSPolicy->childCount() > 0 ) {
        QStringList v_sOCKSPolicy;
        QListViewItemIterator it( m_running->SOCKSPolicy );
        while ( it.current() ) {
            QString s_sOCKSPolicy = it.current()->text(0) + " " + it.current()->text(1) + ":" + it.current()->text(2);
            v_sOCKSPolicy.append(s_sOCKSPolicy);
	        ++it;
        }
        TorkConfig::setSOCKSPolicy( v_sOCKSPolicy );
    }else
        TorkConfig::setSOCKSPolicy( "" );

    //Filter Rules
    if ( m_filtertable->FilterRules->childCount() > 0 ) {
        QStringList v_filterRules;
        QListViewItemIterator it( m_filtertable->FilterRules );
        while ( it.current() ) {
            QString s_filterRules = it.current()->text(0) + "%:%" + it.current()->text(1)+ "%:%" + it.current()->text(2);
            v_filterRules.append(s_filterRules);
	        ++it;
        }
        TorkConfig::setFilterRules( v_filterRules );
    }else
        TorkConfig::setFilterRules( "" );

    //Reachable Address
    if ( m_firewallsproxies->ReachableAddresses->childCount() > 0 ) {
        QStringList v_reachableAddresses;
        QListViewItemIterator it( m_firewallsproxies->ReachableAddresses );
        while ( it.current() ) {
            QString s_reachableAddresses = "*:" + it.current()->text(0);
            v_reachableAddresses.append(s_reachableAddresses);
	        ++it;
        }
        TorkConfig::setReachableAddresses( v_reachableAddresses );
    }else
        TorkConfig::setReachableAddresses( "" );

    //Bridge Addresses
    if ( m_firewallsproxies->Bridge->childCount() > 0 ) {
        QStringList v_bridges;
        QListViewItemIterator it( m_firewallsproxies->Bridge );
        while ( it.current() ) {
            QString s_bridges = it.current()->text(0) + ":" + it.current()->text(1)
                                + " " + it.current()->text(2);
            v_bridges.append(s_bridges);
	        ++it;
        }
        TorkConfig::setBridge( v_bridges );
    }else
        TorkConfig::setBridge( QStringList() );

    //Hidden Services
//     if ( m_myhidden->servicesList->childCount() > 0 ) {
//         QStringList v_hiddenServices;
//         QListViewItemIterator it( m_myhidden->servicesList );
//         while ( it.current() ) {
//             QString s_hiddenServices = it.current()->text(0) + "\n" + it.current()->text(1)
//                                         + "\n" + it.current()->text(2) + "\n" + it.current()->text(3)
//                                         + "\n" + it.current()->text(4)+ "\n" + it.current()->text(5);
//             v_hiddenServices.append(s_hiddenServices);
// 	        ++it;
//         }
//         TorkConfig::setHiddenServices( v_hiddenServices );
//     }else
//         TorkConfig::setHiddenServices( "" );

    //Bandwidth Slots
    if ( m_maxmin->timedBandwidthList->childCount() > 0 ) {
        QStringList v_bandwidthSlots;
        QListViewItemIterator it( m_maxmin->timedBandwidthList );
        while ( it.current() ) {
            QString s_bandwidthSlots = it.current()->text(0) + "\n" + it.current()->text(1)
                                        + "\n" + it.current()->text(2) + "\n" + it.current()->text(3)
                                        + "\n" + it.current()->text(4);
            v_bandwidthSlots.append(s_bandwidthSlots);
	        ++it;
        }
        TorkConfig::setBandwidthSlots( v_bandwidthSlots );
    }else
        TorkConfig::setBandwidthSlots( "" );

    // If we are to use bridges then TunnelDirConns must be set as well.
    if (m_firewallsproxies->kcfg_UseBridges->isChecked())
      TorkConfig::setTunnelDirConns(true);
    else
      TorkConfig::setTunnelDirConns(false);

    TorkConfig::writeConfig();


    //If you're going to disable this again, explain why here. You have a crap
    //memory.
    //emit settingsChanged(); 
    static_cast<tork*>(m_parent)->applySettingsToRunningTor();
}




#include "configdialog.moc"
