/***************************************************************************
 * $Id: quickconfig.ui.h,v 1.11 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/


#include "torkconfig.h"
#include <klocale.h>
#include <qtooltip.h>
#include <qpopupmenu.h>
#include <kdebug.h>
#include <dcopref.h>
#include <kconfig.h>
#include <kmessagebox.h>
#include <klocale.h>


void QuickConfig::init()
{
    QuickConfigure->setCurrentItem(TorkConfig::quickConfigure());

    if (QuickConfigure->currentItem() >= 6){
        if (QuickConfigure->currentItem() == 6)
            kcfg_RemoteTorAddress->setEnabled(true);
        kcfg_RemoteTorPort->setEnabled(true);
    }else{
        kcfg_RemoteTorAddress->setEnabled(false);
        kcfg_RemoteTorPort->setEnabled(false);
    }

}



void QuickConfig::QuickConfigure_activated( int item)
{
    emit configChanged(item);
    kcfg_RemoteTorAddress->setEnabled(false);

    if (item >= 6){
        if (QuickConfigure->currentItem() == 6)
            kcfg_RemoteTorAddress->setEnabled(true);
        kcfg_RemoteTorPort->setEnabled(true);
/*        kcfg_ApplySettingsToInstance->setEnabled(true);*/
    }else{
        kcfg_RemoteTorPort->setEnabled(false);
//         kcfg_ApplySettingsToInstance->setEnabled(false);
    }
}

void
QuickConfig::destroy()
{

//         configureKonqi();

}

// <connections>
//     <connection>
//         <sender>QuickConfigure</sender>
//         <signal>activated(int)</signal>
//         <receiver>QuickConfig</receiver>
//         <slot>QuickConfigure_activated(int)</slot>
//     </connection>
// </connections>
// <includes>
//     <include location="local" impldecl="in implementation">quickconfig.ui.h</include>
// </includes>
// <signals>
//     <signal>settingsChanged()</signal>
//     <signal>configChanged(const int &amp;)</signal>
// </signals>
// <slots>
//     <slot>QuickConfigure_activated( int item )</slot>
// </slots>
// <functions>
//     <function access="private" specifier="non virtual">init()</function>
//     <function access="private" specifier="non virtual">destroy()</function>
// </functions>
// <layoutdefaults spacing="6" margin="11"/>
// </UI>
