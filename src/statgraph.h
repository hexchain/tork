/***************************************************************************
 * $Id: statgraph.h,v 1.4 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   KCPULoad and KNetLoad are copyright (c) 1999-2000, Markus Gustavsson  *
 *                                       (c) 2002, Ben Burton              *
 *                                       (c) 2004-2005, Diego Pettenò      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef STATGRAPH_H
#define STATGRAPH_H

#include <qobject.h>
#include <qstring.h>
#include <qcolor.h>
#include <qlabel.h>

/**
 * @author Diego 'Flameeyes' Pettenò
 * 
 * @brief Abstraction of the dock's graph class for KNetLoad and KCPULoad.
 *
 * This classes abstracts the access to the graphs for KNetLoad and KCPULoad
 * (and eventually other apps, too), making possible to use them without
 * system trays and so on.
 *
 */
class StatGraph : public QLabel {
Q_OBJECT
public:
//@{
/**
 * @name defaultcolors Default graphs' colors
 * @brief These colors will be used as defaults color for graphs.
 */
	static const QColor defaultBgColor;
	static const QColor defaultReadingColor;
	static const QColor defaultLabelColor;
	static const QColor defaultGridColor;
	static const QColor invalidColor;	///< Color used to pass transparent colors
//@}
	
	enum Style { Lines, Bars, Shades };
protected:
	Style m_style;		///< Style of the graph
	QString m_label;	///< Label to show on the graph (QString::null to not show label)
	
	QColor m_bgColor;	///< Background color for the graph @see defaultBgColor
	QColor m_readingColor;	///< Color for the readings graph
	QColor m_labelColor;	///< Color for the graph's label (if any)
	QColor m_gridColor;	///< Color for the graph's grid (if any)
	
	ushort m_gridPace;	///< Pace at which a the grid is drawn. If 0, the grid is not drawn.
	ushort m_pace;		///< Horizontal pace for readings
	uint m_numReadings;	///< Maximum number of readings (label's width / graph's pace)
	uint m_currReading;	///< Current index in the readings arrays.
	uint *m_readings;	///< Array for readings.
	
	Qt::ButtonState m_button;	///< Button clicked in pressed/released events
public:
	/**
	 * @brief Constructor for a StatGraph instance
	 * @param parent Label where to draw the graph
	 * @param pace X-distance between two reads. This will also influence the number
	 *        of readings which can be added (label's width / pace)
	 * @param gridPace Y-distance between two grid lines. If 0, the grid is not drawn.
	 * @param label Label to show on the graph (QString::null to not show label)
	 * @param bgColor Background color to apply to the graph
	 * @param name Name of the statgraph instance (passed to QObject)
	 *
	 * This constructor initialize the QObject parent class and the
	 * label which will be used to draw the graph on.
	 * Note: it will @b not change the background of the label, nor it will draw
	 * the label.
	 */
	StatGraph(QWidget *parent, ushort pace = 1, ushort gridPace = 0, const QString &label = QString::null, const QColor &bgColor = defaultBgColor, const char *name = "");
	
	virtual ~StatGraph();

	/**
	 * @brief Sets the graph's style
	 */
	void setStyle(Style style)
	{ m_style = style; }
	
protected:
	virtual void resizeEvent(QResizeEvent*);
	virtual void paintEvent(QPaintEvent*);
	virtual void mousePressEvent(QMouseEvent *);
	virtual void mouseReleaseEvent(QMouseEvent *);
	
	/**
	 * @brief Paints the grid on the graph
	 */
	void paintGrid(QPainter &p);
	
	/**
	 * @brief Paints the graph using Shades style
	 */
	void paintShades(QPainter &p);
	
	/**
	 * @brief Paints the graph using Bars style
	 */
	void paintBars(QPainter &p);
	
	/**
	 * @brief Paints the graph using Lines style
	 */
	void paintLines(QPainter &p);
	
	/**
	 * @brief Paints the label on the graph
	 */
	void paintLabel(QPainter &p);
	
	/**
	 * @brief Apply softening algorithm to readings
	 *
	 * This function is called to 'soften' the readings when addPercentReadings() is
	 * called with @c soft parameter true.
	 */
	void softenReadings(uint &reading);
	
public slots:
	/**
	 * @brief REquest a clear of the graph
	 *
	 * This functions resets the readings arrays and redraw completely the graph.
	 */
	void clear();
	
	/**
	 * @brief Sets the label to show on the graph
	 *
	 * This function sets the string to show on the graph.
	 * To disable showLabel, simply pass QString::null as label to show.
	 */
	inline void setLabel(const QString &label)
	{
		m_label = label;
	}
	
	/**
	 * @brief Sets the background color for the graph.
	 * 
	 * This function sets the background color for the graph instance
	 * To set the background to transparent, simply pass invalidColor value.
	 */
	void setBgColor(const QColor &color);
	
	/**
	 * @brief Sets the reading color for the graph.
	 * 
	 * This function sets the reading color for the graph instance.
	 * @note Passing invalidColor to this, made it move to the default color
	 *       as soon as a redraw is requested.
	 */
	inline void setReadingColor(const QColor &color)
	{ m_readingColor = color; }

	/**
	 * @brief Sets the label color for the graph.
	 * 
	 * This function sets the label color for the graph instance.
	 * @note Passing invalidColor to this, made it move to the default color
	 *       as soon as a redraw is requested.
	 */
	inline void setLabelColor(const QColor &color)
	{ m_labelColor = color; }
	
	/**
	 * @brief Sets the grid color for the graph.
	 * 
	 * This function sets the grid color for the graph instance.
	 * @note Passing invalidColor to this, made it move to the default color
	 *       as soon as a redraw is requested.
	 */
	inline void setGridColor(const QColor &color)
	{ m_gridColor = color; }
	
	/**
	 * @brief Sets the grid pace for the graph.
	 * 
	 * This function sets the grid pace for the graph instance.
	 * @note Pass 0 to disable grid painting.
	 */
	inline void setGridPace(const ushort gridpace)
	{ m_gridPace = gridpace; }
	
	/**
	* @brief Add the given reading as the most recent in our list.
	* @param reading The reading
	* @param soft If true, the reading will be aproximated to have a soft curve.
	*
	* The diagram will be updated accordingly.
	*/
	void addPercentReading(uchar reading, bool soft);

signals:
	void clickedLeft();
	void clickedRight();
};

#endif
