/***************************************************************************
*   Copyright (C) 2006 - 2008 Robert Hogan                                *
*   robert@roberthogan.net                                                *
begin                : 2004/02/07
copyright            : (C) Mark Kretschmann
email                : markey@web.de
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TORKCONFIGDIALOG_H
#define TORKCONFIGDIALOG_H


#include <qmap.h>
#include <qvaluelist.h>
#include <kconfigdialog.h>

class QComboBox;
class QGroupBox;
class QVBox;

class TorkConfigDialog : public KConfigDialog
{
    Q_OBJECT

    public:
        TorkConfigDialog( QWidget *parent, const char* name, KConfigSkeleton *config );
        ~TorkConfigDialog();

        void addPage( QWidget *page, const QString &itemName, const QString &pixmapName,
                      const QString &header=QString::null, bool manage=true);

        void showPage( const QCString& page );
        class QuickConfig     *m_quickconfig;
    public slots:
        void showServerOpts(bool state);
    signals:
        void updateServerButton( );
    private slots:
        void updateCustomSettings();
        void enableApply();
        void showHidePages( const int &);
    private:
        class FirewallsProxies  *m_firewallsproxies;
        class FilterTable  *m_filtertable;
        class MaxMin  *m_maxmin;
        class Usability     *m_usability;
        class TorServers     *m_torservers;
        class Running     *m_running;
        class MyServer     *m_myserver;
        class MyHidden     *m_myhidden;
        class Konq     *m_konq;

        QValueList<QWidget*> m_pageList;
        QMap<QString, QString> m_pluginName;
        QMap<QString, QString> m_pluginTorkName;
        
        QWidget* m_parent;
};


#endif // TORKCONFIGDIALOG_H
