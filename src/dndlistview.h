/***************************************************************************
*   Copyright (C) 2006 - 2008 Robert Hogan                                *
*   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/


#ifndef _DNDLISTVIEW_H_
#define _DNDLISTVIEW_H_

#include <qlistview.h>
#include <qdragobject.h>

class QDragEnterEvent;
class QDragDropEvent;


class DndListView : public QListView
{
Q_OBJECT

public:
    DndListView( QWidget * parent = 0, const char * name = 0, WFlags f = 0 );
    QPoint m_mousepos;
    QPoint getMousePos();
    bool recentDropEvent() { return m_recentDrop; };
    void contentsMouseReleaseEvent( QMouseEvent *e );

protected:
    void contentsDragEnterEvent( QDragEnterEvent *e );
    void contentsDragMoveEvent( QDragMoveEvent *e );
    void contentsDragLeaveEvent( QDragLeaveEvent *e );
    void contentsDropEvent( QDropEvent *e );
    void contentsMouseMoveEvent( QMouseEvent *e );
    void contentsMousePressEvent( QMouseEvent *e );
    void mouseReleaseEvent( QMouseEvent *e );
    void timerEvent( QTimerEvent * );
    void viewportPaintEvent( QPaintEvent *e );


private:
    QListViewItem *oldCurrent;
    QListViewItem *dropItem;
/*    QTimer* autoopen_timer;*/
    QPoint presspos;
    bool mousePressed;
    int dropTimer;
    bool m_recentDrop;
signals:

    void attach(const QString &, const QString &);
    void extendCircuit(const QString &, const QString &, bool);
    void createCircuit(const QString &, bool);
    void itemAdded(QListViewItem *);





/*    void dragEnterEvent( QDragEnterEvent *evt );
    void dropEvent( QDropEvent *evt );
    void contentsMousePressEvent( QMouseEvent *evt );
    void contentsMouseMoveEvent( QMouseEvent * );*/
//     QDragObject *dragObject();
// private:
//     bool dragging;


};
#endif // _TORKVIEW_H_
