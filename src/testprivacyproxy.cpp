/****************************************************************************
 ** $Id: testprivacyproxy.cpp,v 1.2 2008/08/20 16:49:31 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/


#include <qsocket.h>
#include <qtextstream.h>
#include "testprivacyproxy.h"
#include "tork.h"
#include "torkconfig.h"

#include <qtimer.h>
#include <klocale.h>
#include <assert.h>
#include <qfile.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <kstandarddirs.h>
#include <qdir.h>


TestPrivoxy::TestPrivoxy( )
{
    // create the socket and connect various of its signals
    socket = new QSocket( this );
    connect( socket, SIGNAL(connected()),
            SLOT(socketConnected()) );
    connect( socket, SIGNAL(connectionClosed()),
            SLOT(socketConnectionClosed()) );
/*    connect( socket, SIGNAL(readyRead()),
            SLOT(socketReadyRead()) );*/
    connect( socket, SIGNAL(error(int)),
            SLOT(socketError(int)) );


}

void TestPrivoxy::connectTo(const QString &host, Q_UINT16 port )
{
    // connect to the server
    socket->connectToHost( host, port );
}

TestPrivoxy::~TestPrivoxy( )
{
}

#include "testprivacyproxy.moc"

