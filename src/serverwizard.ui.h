/***************************************************************************
 ** $Id: serverwizard.ui.h,v 1.4 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
#include <kmessagebox.h>
#include <kdebug.h>
#include <qstringlist.h>
#include "tork.h"
#include "torkconfig.h"
#include "upnpmanager.h"

using namespace bt;
using namespace kt;
int serverType;

void ServerWizard::init()
{
    ForwardPorts->setEnabled(false);

    if (UPnPManager::Manager()->routersDiscovered())
        displayDiscoveredRouters();

    serverName->setText("TorKServer");
	KConfig emailConf( QString::fromLatin1("emaildefaults") );
	emailConf.setGroup(QString::fromLatin1("Defaults"));
	QString profile = QString::fromLatin1("PROFILE_");
	profile += emailConf.readEntry(QString::fromLatin1("Profile"), QString::fromLatin1("Default"));
	emailConf.setGroup(profile);

	contactMail->setText(emailConf.readEntry(QString::fromLatin1("EmailAddress")));

}


void ServerWizard::displayDiscoveredRouters()
{
    ForwardPorts->setEnabled(true);
    ForwardPorts->setChecked(true);

    QString routers;
    QStringList routerList;
    routerList = UPnPManager::Manager()->discoveredRoutersNameList();
	for ( QStringList::Iterator it = routerList.begin(); it != routerList.end(); ++it )
	{
		if ((*it).isEmpty())
			continue;
        routers += "- <b>";
        routers += (*it);
        routers += "</b><br>";
    }

    QString routerText = i18n("Make Tor Accessible on the Following Routers:<p> %1").arg(routers);
    RoutersFound->setText(routerText);

}
void
ServerWizard::showPage( QWidget *w ) //virtual
{
    QWizard::showPage( w );


    if (currentPage() == ServerReachability)
        setFinishEnabled ( ServerReachability,true );
}

void
ServerWizard::setServerType( int id ) //virtual
{
    serverType = id;
}

void
ServerWizard::accept()
{
    TorkConfig::setNickName(serverName->text());
    TorkConfig::setContactInfo(contactMail->text());
    TorkConfig::setRunFirstServerWizard(false);
    TorkConfig::setForwardPorts(ForwardPorts->isChecked());
    TorkConfig::writeConfig();
    emit setUpServer(serverType);
    QDialog::accept();

}

void
ServerWizard::reject()
{
    QDialog::reject();
}
