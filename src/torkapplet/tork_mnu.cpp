/***************************************************************************
** $Id: tork_mnu.cpp,v 1.7 2008/07/31 19:56:29 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include <qdir.h>
#include <qfileinfo.h>
#include <qstringlist.h>

#include <kapplication.h>
#include <kglobal.h>
#include <kiconloader.h>
#include <kio/global.h>
#include <klocale.h>
#include <krun.h>
#include <kshell.h>
#include <ksimpleconfig.h>
#include <kstandarddirs.h>
#include <dcopref.h>
#include <dcopclient.h>
#include <kdebug.h>
#include <klocale.h>
#include <krun.h>

#include "tork_mnu.h"
#include "../functions.h"

K_EXPORT_KICKER_MENUEXT(tork, TorkMenu)

QValueVector<QString> torkifyApp(8);


TorkMenu::TorkMenu(QWidget *parent, const char *name, const QStringList& /* args */)
    : KPanelMenu("", parent, name),m_kdestate(true)
{
    p_dcopServer= new DCOPClient();
    p_dcopServer->attach ();

}

TorkMenu::~TorkMenu()
{
    KGlobal::locale()->removeCatalogue("libkickermenu_tork");
    p_dcopServer->detach();
    delete p_dcopServer;

}

void TorkMenu::initialize()
{
    if (initialized())
    {
        clear();
    }
    else
    {
        kapp->iconLoader()->addAppDir("tork");
    }

    setInitialized(true);

    torkifyApp[5] = "Konversation";
    torkifyApp[2] = "Kopete";
    torkifyApp[4] = "Gaim";
    torkifyApp[3] = "Pidgin";
    torkifyApp[6] = "Konsole";
    torkifyApp[1] = "Opera";
    torkifyApp[0] = "Firefox";
    torkifyApp[7] = "Email";

    QStringList paths = getProgramList();


    if (paths.contains("firefox"))
        insertItem( SmallIcon("tork_firefox"), i18n( "Anonymous Firefox" ), 0,1 );
    if (paths.contains("opera"))
        insertItem( SmallIcon("tork_opera"), i18n( "Anonymous Opera" ), 1,2 );
    if (paths.contains("kopete"))
        insertItem( SmallIcon("kopete"), i18n( "Anonymous Kopete" ), 2,3 );
    if (paths.contains("pidgin"))
        insertItem( SmallIcon("pidgin"), i18n( "Anonymous Pidgin" ), 3,4 );
    if (paths.contains("gaim"))
        insertItem( SmallIcon("gaim"), i18n( "Anonymous Gaim" ), 4,5 );
    if (paths.contains("konversation"))
        insertItem( SmallIcon("konversation"), i18n( "Anonymous Konversation" ), 5,6 );
    if (paths.contains("konsole"))
        insertItem( SmallIcon("tork_konsole"), i18n( "Anonymous Konsole Session" ), 6,7 );
    insertSeparator();

    
    insertItem( SmallIcon("tork_mail"), i18n( "Send Anonymous Email" ), 7,8 );

    insertSeparator();
    
    insertItem( SmallIcon("tork_konqueroron"), i18n( "Anonymize KDE" ), 9,9 );

    connect( this, SIGNAL( aboutToShow() ), SLOT( showPopup() ));

}

void TorkMenu::showPopup()
{
    bool anonymized = false;


    if (p_dcopServer->isApplicationRegistered ("tork"))
    {
        DCOPRef tork( "tork", "DCOPTork" );
        anonymized = tork.call( "getKDESetting" );
    }

    if (anonymized)
        changeItem(9, SmallIcon("tork_konqueroroff"), i18n( "De-Anonymize KDE" ));
    else
        changeItem(9, SmallIcon("tork_konqueroron"), i18n( "Anonymize KDE" ));


}

void TorkMenu::slotExec(int id)
{

    m_torkrunning = false;
    if (p_dcopServer->isApplicationRegistered ("tork"))
            m_torkrunning = true;
    

	switch(id)
	{
	case 9:
		anonymizeKDE(); break;
	default:
		anonymousApp(id);break;
	}

}

void TorkMenu::anonymousApp(int id)
{
    
    QString app = torkifyApp[id];
    app.prepend("anonymous");

    if (m_torkrunning){
        DCOPRef("tork*", "DCOPTork").send("startEverything");
        DCOPRef("tork*", "DCOPTork").send(QCString(app));
    }else
        KRun::runCommand( QString("tork --%1").arg(app), "tork", "tork" );

}


void TorkMenu::anonymizeKDE()
{
    if (m_torkrunning){
        DCOPRef("tork*", "DCOPTork").send("startEverything");
        DCOPRef("tork*", "DCOPTork").send("toggleKDESetting");
    }else
        KRun::runCommand( "tork --toggleKDE", "tork", "tork" );

//     if (m_kdestate)
//         changeItem(9, SmallIcon("tork_konqueroroff"), i18n( "De-Anonymize KDE" ));
//     else
//         changeItem(9, SmallIcon("tork_konqueroron"), i18n( "Anonymize KDE" ));
// 
//     m_kdestate = !m_kdestate;

}

QStringList TorkMenu::getProgramList()
{

    //Check for the existence of anonymizable/helper programs first.
    QStringList programList;
    programList << "firefox" << "kopete" << "gaim" <<
                               "pidgin" << "opera" << "konversation";

    return findPrograms(programList);
}

#include "tork_mnu.moc"
