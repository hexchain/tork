/***************************************************************************
** $Id: tork_mnu.h,v 1.5 2008/07/31 19:56:29 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/


#ifndef _tork_mnu_h_
#define _tork_mnu_h_

#include <qvaluevector.h>

#include <kpanelmenu.h>
#include <klibloader.h>
#include <dcopclient.h>


class TorkMenu : public KPanelMenu/*, public KPReloadObject*/
{
    Q_OBJECT

public:
    TorkMenu(QWidget *parent, const char *name, const QStringList& /* args */);
    ~TorkMenu();
    void anonymousApp(int id);
    void anonymizeKDE();
    bool m_kdestate;
    bool m_torkrunning;
    DCOPClient* p_dcopServer;
    QStringList getProgramList();
protected slots:
    void initialize();
    void slotExec(int id);
    void showPopup();
};

#endif

