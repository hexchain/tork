/***************************************************************************
 ** $Id: mixminion.ui.h,v 1.9 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
#include <kprocio.h>
#include <kmessagebox.h>
#include <kprogress.h>
#include <kdebug.h>
#include <qregexp.h>
#include <qtimer.h>
#include "torkconfig.h"
#include <cstdlib>

KProgressDialog* progressDialog;
QString output;

void MixMinionClient::init()
{
}

void MixMinionClient::send_clicked()
{

    if (toAddress->text().isEmpty()){
        KMessageBox::information (this, i18n("Emails are usually sent to someone!"), "Umm.");
        return;
    }
    output = "";
    mailLayout->setEnabled(false);

    KProcIO* mixminionproc = new KProcIO();
    mixminionproc->setUseShell(TRUE);

	QString curpath = (QString) getenv("PATH");
	mixminionproc->setEnvironment("PATH",curpath + ":/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin");

    mixminionproc->setEnvironment("http_proxy",QString("%1:%2").arg(TorkConfig::konqHttpProxy())
                                  .arg(TorkConfig::konqHttpProxyPort()));
    mixminionproc->setEnvironment("https_proxy",QString("%1:%2").arg(TorkConfig::konqHttpsProxy())
                                  .arg(TorkConfig::konqHttpsProxyPort()));

    *mixminionproc << "printf '" << messageBody->text() <<"' | mixminion  send -t " << toAddress->text() 
                << " --subject '" << subject->text() << "'";

 	connect( mixminionproc, SIGNAL(processExited(KProcess *)),
 			SLOT(mixminionprocExited(KProcess *)) );
	connect( mixminionproc, SIGNAL(readReady(KProcIO *)),
		SLOT(receivedMixminionOutput(KProcIO *)) );

    progressDialog = new KProgressDialog( this, "progress_dialog", QString::null, "Sending Anonymous Mail Message..", false );

    progressDialog->setPlainCaption( i18n( "Sending Anonymous Mail Message.." ) );

    progressDialog->progressBar()->setTotalSteps( 00 );
    progressDialog->progressBar()->setPercentageVisible( false );

    progressDialog->setMinimumDuration( 500 );
    progressDialog->show();
    QTimer* timer = new QTimer( this );
    connect( timer, SIGNAL( timeout() ), this, SLOT( slotProg() ) );

    timer->start( 200, FALSE );

	mixminionproc->start(KProcIO::NotifyOnExit,KProcIO::All);

}

void MixMinionClient::slotProg()
{

    if (progressDialog)
        progressDialog->progressBar()->setProgress(progressDialog->progressBar()->progress() + 4 );
}

void MixMinionClient::receivedMixminionOutput(KProcIO *mixminionproc)
{

    int pos;
    QString item2;
	while ((mixminionproc) && ((pos = (mixminionproc->readln(item2,true))) != -1)) {
            item2.replace(QRegExp("^[^#]+\\]"),"");
            progressDialog->setLabel( item2.stripWhiteSpace());
            progressDialog->progressBar()->setProgress(progressDialog->progressBar()->progress() + 4 );
            output += item2;
            output.append("<br>");
            mixminionproc->ackRead();
    }
}


void MixMinionClient::mixminionprocExited(KProcess *proc)
{
    QString caption;
    QString message;
    delete progressDialog;
    progressDialog = 0;

    if (proc->exitStatus() == 0){
        caption = i18n("Email Successfully Dispatched!");
        message = i18n("<p>%1<br>").arg(output);
    }else{
        caption = i18n("There was a problem!");
        message = i18n("<p>%1<br>").arg(output);

    }

    KMessageBox::information (this, message, caption);

    delete this;
}
