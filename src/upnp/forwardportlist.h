/***************************************************************************
 *   Copyright (C) 2005 by Joris Guisson                                   *
 *   joris.guisson@gmail.com                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.          *
 ***************************************************************************/
#ifndef FORWARDPORTLIST_H
#define FORWARDPORTLIST_H

#include <qvaluelist.h>
#include "../constants.h"
#include "portlist.h"

namespace net
{


	struct ForwardPort
	{
		bt::Uint16 extnumber;
        bt::Uint16 intnumber;
		Protocol proto;
		bool forward;
		
		ForwardPort();
		ForwardPort(bt::Uint16 extnumber,bt::Uint16 intnumber,Protocol proto,bool forward);
		ForwardPort(const ForwardPort & p);
		
		bool operator == (const ForwardPort & p) const;
	};
	
	/**
	 * Listener class for the ForwardPortList. 
	 */
	class ForwardPortListener
	{
	public:
		/**
		 * A port has been added.
		 * @param port The port
		 */
		virtual void portAdded(const ForwardPort & port) = 0;
		
		/**
		 * A port has been removed
		 * @param port The port
		 */
		virtual void portRemoved(const ForwardPort & port) = 0;
	};

	/**
	 * @author Joris Guisson <joris.guisson@gmail.com>
	 * 
	 * List of ports which are currently being used.
	 * 
	*/
	class ForwardPortList : public QValueList<ForwardPort>
	{
		ForwardPortListener* lst;
	public:
		ForwardPortList();
		virtual ~ForwardPortList();

		/**
		 * When a port is in use, this function needs to be called.
		 * @param number ForwardPort number
		 * @param proto Protocol
		 * @param forward Wether or not it needs to be forwarded
		 */
		void addNewForwardPort(bt::Uint16 extnumber,bt::Uint16 intnumber,Protocol proto,bool forward);
		
		/**
		 * Needs to be called when a port is not being using anymore.
		 * @param number ForwardPort number
		 * @param proto Protocol
		 */
		void removeForwardPort(bt::Uint16 extnumber,bt::Uint16 intnumber,Protocol proto);
		
		/**
		 * Set the port listener.
		 * @param pl ForwardPort listener
		 */
		void setListener(ForwardPortListener* pl) {lst = pl;}
	};

}

#endif
