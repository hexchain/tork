/***************************************************************************
 *   Copyright (C) 2005 by Joris Guisson                                   *
 *   joris.guisson@gmail.com                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.          *
 ***************************************************************************/
#include "forwardportlist.h"
#include <kdebug.h>
namespace net
{
	ForwardPort::ForwardPort() : extnumber(0),intnumber(0),proto(TCP),forward(false)
	{
	}
	
	ForwardPort::ForwardPort(bt::Uint16 extnumber,bt::Uint16 intnumber,Protocol proto,bool forward)
	: extnumber(extnumber),intnumber(intnumber),proto(proto),forward(forward)
	{
	}
	
	ForwardPort::ForwardPort(const ForwardPort & p) : extnumber(p.extnumber),
                             intnumber(p.intnumber),proto(p.proto),forward(p.forward)
	{
	}


	bool ForwardPort::operator == (const ForwardPort & p) const
	{
		return extnumber == p.extnumber && intnumber == p.intnumber && proto == p.proto;
	}

	ForwardPortList::ForwardPortList() : lst(0)
	{}


	ForwardPortList::~ForwardPortList()
	{}

		
	void ForwardPortList::addNewForwardPort(bt::Uint16 extnumber,bt::Uint16 intnumber,Protocol proto,
                                            bool forward)
	{
        kdDebug() << "adding forward port" << endl;

		ForwardPort p = ForwardPort(extnumber,intnumber,proto,forward);
		append(p);
		if (lst)
			lst->portAdded(p);
        kdDebug() << "added forward port" << endl;

	}
		
		
	void ForwardPortList::removeForwardPort(bt::Uint16 extnumber,bt::Uint16 intnumber,Protocol proto)
	{
        kdDebug() << "removing forward port" << endl;
		ForwardPortList::iterator itr = find(ForwardPort(extnumber,intnumber,proto,false));
		if (itr == end())
			return;
		
		if (lst)
			lst->portRemoved(*itr);
		
		erase(itr);
        kdDebug() << "removed forward port" << endl;

	}
		
	

}
