/***************************************************************************
 *   Copyright (C) 2005 Novell, Inc.                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA           *
 ***************************************************************************/

#ifndef HITWIDGET_H
#define HITWIDGET_H

#include <hitwidget_layout.h>
#include "kwidgetlistbox.h"

class QScrollView;
// class KonqFileTip;

class HitWidget : public HitWidgetLayout
{
    Q_OBJECT
  public:
    HitWidget(QString uri, QString mimetype, KWidgetListbox *parent = 0, const char *name = 0);
    ~HitWidget();

    void setCollapsed(bool);
    bool isCollapsed() const;
    void setIcon(QString name);

    void setDescriptionText(QString text);
    void setPropertiesText(QString text);
    void setUri(const QString uri);
    QString uri() const;
    QString mimetype() const;

    void insertHeaderWidget( int index, QWidget * widget);
    void insertTextWidget( int index, QWidget * widget);
    void insertHitWidget( int index, QWidget * widget);
    void insertHitSpacing( int index, int size);

    virtual void adjustSize();
    virtual bool eventFilter(QObject*, QEvent*);

    QMap<QString,QString> shellTitles;

  signals:
    void uncollapsed(HitWidget*);

  private slots:
    void toggleCollapsed();
    void changeTerminalUrl(int no);

  private:
    KWidgetListbox* qsv;
    QString m_uri, m_mimetype;
/*    KonqFileTip* pFileTip;*/
    bool m_collapsed, m_was_collapsed;
    QString m_icon;
};

#endif
