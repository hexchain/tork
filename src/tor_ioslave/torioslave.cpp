/***************************************************************************
** $Id: torioslave.cpp,v 1.7 2008/07/31 19:56:29 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

#include <qcstring.h>
#include <qsocket.h>
#include <qdatetime.h>
#include <qbitarray.h>

#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>

#include <kapplication.h>
#include <kdebug.h>
#include <kmessagebox.h>
#include <kinstance.h>
#include <kglobal.h>
#include <kstandarddirs.h>
#include <klocale.h>
#include <kurl.h>
#include <ksock.h>
#include <dcopref.h>
#include <dcopclient.h>
#include <kdebug.h>
#include <klocale.h>
#include <krun.h>
#include <kprocio.h>

#include "torioslave.h"

using namespace KIO;


kio_torProtocol::kio_torProtocol(const QCString &pool_socket, const QCString &app_socket)
    : SlaveBase("kio_tor", pool_socket, app_socket)
{
}


kio_torProtocol::~kio_torProtocol()
{
}


void kio_torProtocol::get(const KURL& url )
{

    QString wait;
    QByteArray output;

    QTextStream os( output, IO_WriteOnly );
    os.setEncoding( QTextStream::Latin1 ); // In fast ASCII

    QString cleanedurl = url.prettyURL().replace("tor://","");
    cleanedurl = cleanedurl.replace("tor:/","");
    cleanedurl = cleanedurl.replace("tor:","");
    cleanedurl = cleanedurl.replace(" ","");
    bool cantdo = false;

    if (cleanedurl.contains(".txt"))
        cantdo = true;

    if (cleanedurl.contains("://") && (cleanedurl.left(4) != "http"))
        cantdo = true;

    if (cantdo){

        os << QString("<html><head><title>Tor</title></head><body>%1 is not a valid URL for this feature. Websites only I'm afraid. And no file extensions either!</body></html>").arg(cleanedurl);
    
        data( output );
        finished();
        return;
    }

    if ((cleanedurl.left(7) != "http://") && (cleanedurl.left(8) != "https://"))
        cleanedurl.prepend("http://");

    bool m_torkrunning = false;
    bool anonymized = false;

    DCOPClient* p_dcopServer= new DCOPClient();
    p_dcopServer->attach ();

    if (p_dcopServer->isApplicationRegistered ("tork")){
            m_torkrunning = true;
            DCOPRef tork( "tork", "DCOPTork" );
            anonymized = tork.call( "getKDESetting" );
    }

    

    if (m_torkrunning){
        DCOPRef("tork*", "DCOPTork").send("startEverything");
        if (!anonymized)
            DCOPRef("tork*", "DCOPTork").send("toggleKDESetting");
        wait = "3";
    }else{
        KProcIO *whichproc = new KProcIO();
        whichproc->setUseShell(TRUE);
        QString whichCommand="tork --toggleKDE";
    
        *whichproc<<whichCommand;
    
        whichproc->start(KProcIO::NotifyOnExit,TRUE);

        //KRun::runCommand( "tork --toggleKDE");
        wait = "10";
    }


    os << QString("<html><head><title>Tor</title><META HTTP-EQUIV='Refresh' CONTENT='%1; URL=%2'></head><body>Will load %3 anonymously in a moment.</body></html>").arg(wait).arg(cleanedurl).arg(cleanedurl);

    data( output );
    finished();

}


void kio_torProtocol::mimetype(const KURL & /*url*/)
{
    mimeType("text/html");
    finished();
}


extern "C"
{
    int kdemain(int argc, char **argv)
    {
        KInstance instance( "kio_tor" );
        
        kdDebug(7101) << "*** Starting kio_tor " << endl;
        
        if (argc != 4) {
            kdDebug(7101) << "Usage: kio_tor  protocol domain-socket1 domain-socket2" << endl;
            exit(-1);
        }
        
        kio_torProtocol slave(argv[2], argv[3]);
        slave.dispatchLoop();
        
        kdDebug(7101) << "*** kio_tor Done" << endl;
        return 0;
    }
} 
