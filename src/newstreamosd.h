/* 
 *
 * $Id: newstreamosd.h,v 1.7 2008/08/20 16:49:23 hoganrobert Exp $
 * Copyright (C) 2005 Sebastian Trueg <trueg@k3b.org>
 *
 * This file is part of the K3b project.
 * Copyright (C) 1998-2005 Sebastian Trueg <trueg@k3b.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * See the file "COPYING" for the exact licensing terms.
 */

#ifndef _K3B_JOB_PROGRESS_OSD_H_
#define _K3B_JOB_PROGRESS_OSD_H_

#include <qwidget.h>
#include <qpixmap.h>
#include <qlistview.h>
#include <qtoolbutton.h>

#include "statgraph.h"
#include "chart.h"

class QPaintEvent;
class QMouseEvent;
class KConfigBase;
class QHBox;
class QListView;
class QToolButton;
class StatGraph;
class Chart;
class torkView;
/**
 * An OSD displaying a text and a progress bar.
 *
 * Insprired by Amarok's OSD (I also took a bit of their code. :)
 */
class StreamOSD : public QWidget
{
  Q_OBJECT

 public:
  StreamOSD( torkView* parent = 0, bool tortraffic = true , const char* name = 0);
  ~StreamOSD();

  int screen() const { return m_screen; }
  const QPoint& position() const { return m_position; }

  void readSettings( KConfigBase* );
  void saveSettings( KConfigBase* );

 signals:
  void requestHideMonitor();
  void requestToggleKDE();
  void requestChangeID();
  void closeStream(const QString &);

 public slots:
  void setScreen( int );
  void setText( const QString& );
  void setProgress( int );
  void slotHideMonitor();
  void slotToggleKDE();
  void slotChangeID();

  /**
   * The position refers to one of the corners of the widget
   * regarding on the value of the x and y coordinate.
   * If for example the x coordinate is bigger than half the screen
   * width it refers to the left edge of the widget.
   */
  void setPosition( const QPoint& );

  void contextMenuRequested( QListViewItem *, const QPoint & point, int );
  void slotCloseStream();

 protected:
  void paintEvent( QPaintEvent* );
  void mousePressEvent( QMouseEvent* );
  void mouseReleaseEvent( QMouseEvent* );
  void mouseMoveEvent( QMouseEvent* );
  void renderOSD();
  void renderNonTorOSD();
  void refresh();
  void reposition( QSize size = QSize() );

 public:
  QListView* infoList;
  StatGraph* m_graphIn;
  StatGraph* m_graphOut;
  Chart* chart;
  QToolButton* toggleKDE;
  QToolButton* changeID;
 private:
  /**
   * Ensure that the position is inside m_screen 
   */
  QPoint fixupPosition( const QPoint& p );
  static const int s_outerMargin = 15;

  QPixmap m_osdBuffer;
  bool m_dirty;
  QString m_text;
  int m_progress;
  bool m_dragging;
  QPoint m_dragOffset;
  int m_screen;
  QPoint m_position;
  torkView* m_parent;
  bool m_tortraffic;

};

#endif
