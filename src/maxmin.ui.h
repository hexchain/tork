/***************************************************************************
** $Id: maxmin.ui.h,v 1.12 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
#include "torkconfig.h"
#include <klocale.h>
#include <kmessagebox.h>
#include <qtooltip.h>
#include <qpopupmenu.h>

void MaxMin::init()
{

    QStringList bandwidthSlots = TorkConfig::bandwidthSlots();
	for ( QStringList::Iterator it = bandwidthSlots.begin(); it != bandwidthSlots.end(); ++it )
	{
		if ((*it).isEmpty())
			continue;

        new QListViewItem(timedBandwidthList,(*it).section("\n",-5,-5),
            (*it).section("\n",-4,-4),(*it).section("\n",-3,-3),(*it).section("\n",-2,-2),(*it).section("\n",-1));
    }

}




void MaxMin::kcfg_DefaultMaxMinOptions_toggled( bool state )
{
    MaxMinOptions->setEnabled(!state);
    kcfg_UseScheduledBandwidth->setEnabled(!state);
    if (kcfg_UseScheduledBandwidth->isChecked())
        TimedBandwidth->setEnabled(!state);
    else
        TimedBandwidth->setEnabled(false);

    if (state){
        kcfg_BandwidthRate->setValue(TorkConfig::defaultBandwidthRate());
        kcfg_BandwidthBurst->setValue(TorkConfig::defaultBandwidthBurst());
        kcfg_MaxAdvertisedBandwidth->setValue(TorkConfig::defaultMaxAdvertisedBandwidth());
    }

}


void MaxMin::kcfg_BandwidthRate_valueChanged( int  newValue)
{
    if (newValue > kcfg_BandwidthBurst->value()){
        KMessageBox::information(this, "Largest Chunk of Bandwidth should be at least equal to the Bandwidth Rate");
        kcfg_BandwidthBurst->setValue(kcfg_BandwidthRate->value());
    }
}



void MaxMin::kcfg_BandwidthBurst_valueChanged( int  newValue)
{
    if (newValue < kcfg_BandwidthRate->value()){
        KMessageBox::information(this, "Largest Chunk of Bandwidth should be at least equal to the Bandwidth Rate");
        kcfg_BandwidthBurst->setValue(kcfg_BandwidthRate->value());
    }
}


void MaxMin::pushButton1_clicked()
{

    QString bwtime = theTime->time().toString();
    QString max = QString("%1").arg(kcfg_BandwidthRate->value());
    QString burst = QString("%1").arg(kcfg_BandwidthBurst->value());
    QString adv = QString("%1").arg(kcfg_MaxAdvertisedBandwidth->value());

    new QListViewItem(timedBandwidthList,bwtime,theDay->currentText(),max,burst,adv);

}

void MaxMin::timedBandwidthList_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{

    QPopupMenu *menu = new QPopupMenu( timedBandwidthList );

    menu->clear();
    menu->insertItem( "Delete Entry", this,SLOT(slotDeleteEntry()) );
    menu->popup( point );

}


void MaxMin::slotDeleteEntry( )
{

    QListViewItemIterator it(timedBandwidthList, QListViewItemIterator::Selected);
    while ( it.current() ) {
        if (timedBandwidthList->isSelected( it.current()))
            delete it.current();
        ++it;
    }


}


void MaxMin::kcfg_UseScheduledBandwidth_toggled( bool state)
{
    TimedBandwidth->setEnabled(state);
}
