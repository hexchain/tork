/***************************************************************************
 * $Id: update.h,v 1.13 2008/12/08 19:39:02 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/


#ifndef _UPDATE_H_
#define _UPDATE_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <kapplication.h>
#include <kdirlister.h> //stack allocated
#include <kurl.h>       //stack allocated
#include <kresolver.h> // namespace
#include <khtml_part.h>


namespace DOM {
  class Node;
}


using namespace KNetwork;

class KURL;
class KProcess;
class KURLRequester;
class KProgressDialog;
class KProcIO;
class KHTMLPart;

/**
 * This class serves as the main window for TorkUpdate.  It handles the
 * menus, toolbars, and status bars.
 *
 * @short Main window class
 * @author $AUTHOR <$EMAIL>
 * @version $APP_VERSION
 */
class TorkUpdate : public QWidget
{
  Q_OBJECT

  public:

    TorkUpdate(QWidget *parent, const char *name=0);

    /**
     * Default Destructor
     */
    virtual ~TorkUpdate();



public slots:
    void checkForNewTorDirectly(bool alpha);
    void checkForNewDanteDirectly();
    void checkForNewPrivoxyDirectly();
    void checkForNewTorkDirectly();
    void downloadThttpd();
    void downloadMixminion();


private slots:
    void slotProg();
    void torkInstallationExited(KProcess *arkollonproc);
    void resolved(KResolverResults result);
    void parseSFPage();
    void parseSFPageHack();
    void updateCanceled();
private:
    void getLatestVersionFromSF(KURL url);
    void getLatestVersionFromSFHack(KURL url);
    void  downloadComponent(QString component, QString version, QString extension);
    void startProgressDialog( const QString & text );
    void checkInternet();
    void connectToHost();
    void checkForNewTork();
    void getVersionFromLink( const DOM::Node &n );
    void completedSearchForUpdates(const QString &component, const QString &extension);
    double numericizeVersion(QString &version);
    QString getMirror();
    KProcess *childproc;
    KProcIO *versionproc;
    QString tempFileName;
    QString errorMessage;
    QString pidFileName;
    KProgressDialog *progressDialog;
    QTimer *timer;
    bool upgradeinprogress;
    bool checkingDirectly;
    double highestsofarnumeric;
    QString highestsofarraw;
    QString highestsofarfilename;
    KResolver m_resolver;
    QString currentTorVersion;
    bool m_alpha;
    KHTMLPart *filelist;
private:
    KDirLister tor_lister;
    KURL       tor_url;
    KDirLister tork_lister;
    KURL       tork_url;
    KURL       m_url;

};

#endif // _TorkUpdate_H_
