/***************************************************************************
 * $Id: usability.ui.h,v 1.12 2008/07/31 19:56:28 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/


#include "torkconfig.h"
#include "portsandnames.h"
#include <klocale.h>
#include <qtooltip.h>
#include <qpopupmenu.h>
#include <kdebug.h>


void Usability::init()
{
    QListViewItemIterator it(LongLivedPorts);
    while ( it.current() ) {
        int i;
        QString port;
        for (i = 0; _port_name[i].port; ++i) {
            portsandnames_t *pics = &_port_name[i];
            if (it.current()->text(0) == pics->name){
                port = pics->port;
                continue;
            }
        }

        if (TorkConfig::longLivedPorts().contains(port))
            LongLivedPorts->setSelected(*it,true);
        ++it;
    }

    QStringList existingTracks = TorkConfig::trackHostExits();
    QListViewItem *tm;
	for ( QStringList::Iterator it = existingTracks.begin(); it != existingTracks.end(); ++it )
	{
		if ((*it).isEmpty())
			continue;
        tm = new QListViewItem(TrackHostExits,(*it));
    }

}
void Usability::LongLivedPorts_selectionChanged()
{
    emit settingsChanged();
}




void Usability::PushAdd_clicked()
{

    new QListViewItem(TrackHostExits,NewHost->text());
}



void Usability::TrackHostExits_contextMenuRequested( QListViewItem *, const QPoint &point, int )
{

    QPopupMenu *menu = new QPopupMenu( TrackHostExits );

    menu->clear();
    menu->insertItem( "Delete Entry", this,SLOT(slotDeleteEntry()) );
    menu->popup( point );

}


void Usability::slotDeleteEntry( )
{

    QListViewItemIterator it(TrackHostExits, QListViewItemIterator::Selected);
    while ( it.current() ) {
        if (TrackHostExits->isSelected( it.current()))
            delete it.current();
        ++it;
    }


}


// <connections>
//     <connection>
//         <sender>LongLivedPorts</sender>
//         <signal>selectionChanged()</signal>
//         <receiver>Usability</receiver>
//         <slot>LongLivedPorts_selectionChanged()</slot>
//     </connection>
//     <connection>
//         <sender>TrackHostExits</sender>
//         <signal>contextMenuRequested(QListViewItem*,const QPoint&amp;,int)</signal>
//         <receiver>Usability</receiver>
//         <slot>TrackHostExits_contextMenuRequested(QListViewItem*,const QPoint&amp;,int)</slot>
//     </connection>
//     <connection>
//         <sender>PushAdd</sender>
//         <signal>clicked()</signal>
//         <receiver>Usability</receiver>
//         <slot>PushAdd_clicked()</slot>
//     </connection>
// </connections>
// <includes>
//     <include location="local" impldecl="in implementation">usability.ui.h</include>
// </includes>
// <signals>
//     <signal>settingsChanged()</signal>
// </signals>
// <slots>
//     <slot>LongLivedPorts_selectionChanged()</slot>
//     <slot>TrackHostExits_contextMenuRequested( QListViewItem *, const QPoint &amp; point, int )</slot>
//     <slot>slotDeleteEntry()</slot>
//     <slot>PushAdd_clicked()</slot>
// </slots>
// <functions>
//     <function access="private" specifier="non virtual">init()</function>
// </functions>
// <layoutdefaults spacing="6" margin="11"/>
// </UI>



void Usability::clearSelected_clicked()
{

    QListViewItemIterator it(LongLivedPorts, QListViewItemIterator::Selected);
    while ( it.current() ) {
        it.current()->listView()->setSelected(it.current(),false);
        ++it;
    }

}
