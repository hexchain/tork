/***************************************************************************
** $Id: tork_plug_in.cpp,v 1.4 2008/07/31 19:56:28 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

/***************************************************************************
                          kget_plug_in.cpp  -  description
                             -------------------
    begin                : Wed Jul  3 22:09:28 CEST 2002
    copyright            : (C) 2002 by Patrick
    email                : pch@valleeurpe.net
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "tork_plug_in.h"

#include <dcopref.h>
#include <kdatastream.h>
#include <kdebug.h>
#include <khtml_part.h>
#include <kiconloader.h>
#include <kglobal.h>
#include <kaction.h>
#include <kinstance.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <kpopupmenu.h>
#include <krun.h>

#include <dom/html_document.h>
#include <dom/html_misc.h>
#include <dom/dom_element.h>

#include <kparts/partmanager.h>

#include <set>


Tork_plug_in::Tork_plug_in( QObject* parent, const char* name )
    : Plugin( parent, name )
{
    QPixmap pix = KGlobal::iconLoader()->loadIcon("tork",
                                                  KIcon::MainToolbar);
    KActionMenu *menu = new KActionMenu( i18n("Anonymity Manager"), pix,
                                         actionCollection(), "tork_menu" );
    menu->setDelayed( false );
    connect( menu->popupMenu(), SIGNAL( aboutToShow() ), SLOT( showPopup() ));

    m_paToggleKDE=new KAction(i18n("Anonymize KDE"),
                                           KGlobal::iconLoader()->loadIconSet("tork_konqueroroff",                                               KIcon::Small),
                                           KShortcut(),
                                           this, SLOT(toggleKDE()),
                                           actionCollection(), "tork_konqueroron" );

    menu->insert( m_paToggleKDE );


    KAction *m_firefox=new KAction(i18n("Re-Open Anonymously with Firefox"),
                                           KGlobal::iconLoader()->loadIconSet("tork_firefox",                                               KIcon::Small),
                                           KShortcut(),
                                           this, SLOT(openWithFirefox()),
                                           actionCollection(), "tork_firefox" );
    menu->insert( m_firefox );

    KAction *m_opera=new KAction(i18n("Re-Open Anonymously with Opera"),
                                           KGlobal::iconLoader()->loadIconSet("tork_opera",                                               KIcon::Small),
                                           KShortcut(),
                                           this, SLOT(openWithOpera()),
                                           actionCollection(), "tork_opera" );
    menu->insert( m_opera );

    p_dcopServer= new DCOPClient();
    p_dcopServer->attach ();
}


Tork_plug_in::~Tork_plug_in()
{
    p_dcopServer->detach();
    delete p_dcopServer;
}


void Tork_plug_in::showPopup()
{
    bool anonymized = false;

    if (p_dcopServer->isApplicationRegistered ("tork"))
    {
        DCOPRef tork( "tork", "DCOPTork" );
        anonymized = tork.call( "getKDESetting" );
    }

    if (anonymized){
        m_paToggleKDE->setIconSet( KGlobal::iconLoader()->loadIconSet("tork_konqueroroff",
                                                  KIcon::Small) );
        m_paToggleKDE->setText( "De-Anonymize KDE" );
    }else{
        m_paToggleKDE->setIconSet( KGlobal::iconLoader()->loadIconSet("tork_konqueroron",
                                                  KIcon::Small) );
        m_paToggleKDE->setText( "Anonymize KDE" );
    }
}

void Tork_plug_in::openWithFirefox()
{

    openWithBrowser("Firefox");

}

void Tork_plug_in::openWithOpera()
{

    openWithBrowser("Opera");

}

void Tork_plug_in::openWithBrowser(const QString &browser)
{
    KHTMLPart *htmlPart = static_cast<KHTMLPart*>( parent() );
    QString url = htmlPart->toplevelURL().url();

    if (p_dcopServer->isApplicationRegistered ("tork")){
        QString function = QString("anonymized%1").arg(browser);
        DCOPRef("tork", "DCOPTork").send("startEverything");
        DCOPRef("tork", "DCOPTork").send(QCString(function),url);
    }else
        KRun::runCommand( QString("tork --anonymous%1 %2").arg(browser).arg(url), "tork", "tork" );

}

void Tork_plug_in::toggleKDE()
{
    if (!p_dcopServer->isApplicationRegistered ("tork"))
        KRun::runCommand("tork --toggleKDE");
    else
    {
        DCOPRef tork( "tork", "DCOPTork" );
        tork.send( "toggleKDESetting");
    }
}


KPluginFactory::KPluginFactory( QObject* parent, const char* name )
        : KLibFactory( parent, name )
{
    s_instance = new KInstance("KPluginFactory");
}

QObject* KPluginFactory::createObject( QObject* parent, const char* name, const char*, const QStringList & )
{
    QObject *obj = new Tork_plug_in( parent, name );
    return obj;
}

KPluginFactory::~KPluginFactory()
{
    delete s_instance;
}

extern "C"
{
    KDE_EXPORT void* init_khtml_tork()
    {
        KGlobal::locale()->insertCatalogue("tork");
        return new KPluginFactory;
    }

}

KInstance* KPluginFactory::s_instance = 0L;

#include "tork_plug_in.moc"
