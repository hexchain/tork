/***************************************************************************
 ** $Id: tork_plug_in.h,v 1.4 2008/07/31 19:56:28 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

/***************************************************************************
                          kget_plug_in.h  -  description
                             -------------------
    begin                : Wed Jul  3 22:09:28 CEST 2002
    copyright            : (C) 2002 by Patrick
    email                : pch@valleeurpe.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef __plugin_tork_plug_in_h
#define __plugin_tork_plug_in_h

#include <kparts/plugin.h>
#include <klibloader.h>
#include <dcopclient.h>
#include <kurl.h>

class KInstance;

class Tork_plug_in : public KParts::Plugin
{
    Q_OBJECT
public:
    Tork_plug_in( QObject* parent = 0, const char* name = 0 );
    KAction *m_paToggleKDE ;
    DCOPClient* p_dcopServer;
    virtual ~Tork_plug_in();

private slots:
    void toggleKDE();
    void openWithBrowser(const QString&);
    void openWithFirefox();
    void openWithOpera();
    void showPopup();
};


class KPluginFactory : public KLibFactory
{
    Q_OBJECT
public:
    KPluginFactory( QObject *parent = 0, const char *name = 0 );
    ~KPluginFactory() ;

    virtual QObject* createObject( QObject* parent = 0, const char* pname = 0,
                                   const char* name = "QObject",
                                   const QStringList &args = QStringList() );

private:
    static KInstance* s_instance;
};

#endif
