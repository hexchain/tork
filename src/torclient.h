/****************************************************************************
 ** $Id: torclient.h,v 1.76 2009/01/17 15:49:08 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of an example program for Qt.  This example
** program may be used, distributed and modified without limitation.
**
*****************************************************************************/

#ifndef _TORCLIENT_H_
#define _TORCLIENT_H_

#include <qsocket.h>
#include <qtextstream.h>
#include <kdebug.h>
#include <kconfigskeleton.h>
#include <qlistview.h>
#include "torkconfig.h"


class KConfigSkeleton;
class KConfigSkeletonItem;

class PrevConfig
    {
    public:
        PrevConfig(): pr(0) {}
        PrevConfig( const QString& name, const QVariant& property )
            : nm(name), pr(property)
        {}
        typedef QValueList<PrevConfig> PrevConfigList;
        QString name() const { return nm; }
        QVariant property() const { return pr; }
        void setName( QString n ) { nm = n; }
        void setProperty( QVariant p ) { pr = p; }
    private:
        QString nm;
        QVariant pr;
    };



class TorClient : public QObject
{
    Q_OBJECT
public:
    TorClient( const QString &host, Q_UINT16 port );

    virtual ~TorClient();


    void sendToServer(const QString &string)
    {
        if (!socket)
            return;
        QTextStream os(socket);
        os << string << "\r\n";
    }

    QStringList currentServerReport()
    {
        return serverReport;
    }

    QStringList currentClientReport()
    {
        return clientReport;
    }

    void bandwidth();
    bool isControllerWorking( );
    void updatePrevConfig(PrevConfig::PrevConfigList previtems);
    void newIdentity();
    void createService(const QString &dir, const QString &port);
    void setBandwidth(const QString &rate, const QString &burst, const QString &max);
    void setGeoIPAvailable(bool set){geoip_db = set;};
    unsigned long int getCurBandwidthRate(){return m_CurBandwidthRate;};
    unsigned long int getCurBandwidthBurst(){return m_CurBandwidthBurst;};
    unsigned long int getCurMaxAdvertisedBandwidth()
                       {return m_CurMaxAdvertisedBandwidth;};
    void getBandwidth();
    void enableDNS( bool set );
    void enableTransPort( bool set );
    void cleanUp();

signals:

    void streamStatusUpdate(const QString &, const QString &,
        const QString &, const QString &, const QString &);
    void ORStatusUpdate(const QString &, const QString &);
    void guardStatusUpdate(const QString &, const QString &);
    void circuitStatusUpdate(const QString &, const QString &,
                             const QString &, const QString &);
    void infoUpdate(const QString &,const QString &, const QString &);
    void bwUpdate(const QString &,const QString &);
    void updateActiveServers(const QStringList &);
    void updateServerStatus(const QString &, const QString &,
                            const QString &,const QString &);
    void fatalError();
    void serverError();
    void displayError(const QString &,const QString &);
    void displayServer(const QString &,const QString &);
    void whatImDoing(const QString &);
    void copyOldConfig();
    void shouldIApplySettings();
    void torConnectionClosed();
    void makeTorkStoppable();
    void warnNoServerInfo();
    void needAlphaVersion( );
    void connectedToTor( );
    void authenticated();
    void streamBwUpdate(const QString &,const QString &,const QString & );
    void setTorCaption(const QString &);
    void processWarning(const QString &,const QString &);
    void processQuestion(const QString &,const QString &);
    void showServerBW(const QString&);
    void updateTrayIcon(const QString&);
    void resolvedAddress(const QString&);
	void authenticationFailed();

public slots:
    void socketReadyRead();
    void applySettingsToRunningTor();
    void attemptAttach(const QString &, const QString &);
    void attemptExtendCircuit(const QString &, const QString &, bool);
    void attemptCreateCircuit(const QString &, bool);
    void attemptCloseStream(const QString &);
    void attemptAttachStreams( bool );
    void attemptCloseCircuit(const QString &);
    void fetchServerInfo(const QString &);
    void fetchServerInfoByNick(const QString &);
    void slotCheckTorNet();
    void authenticate();
    void slotCheckGuards();
    void updateExcludeNodes();
    void updateEntryNodes();
    void updateExitNodes();
    void safeLogging( bool safe);
    void clearNodes();
    void strictExitNodes( bool strict );
    void configureServer( int orPort, int dirPort);
    void closeAllCircuits( QListView* &circuitList);
    void allowPlainTextPorts( );
    void assignPortToRemove();
    void resolveAddress(const QString &);
    void terminateTor();

private slots:
    void closeConnection()
    {
        socket->close();
        if ( socket->state() == QSocket::Closing ) {
            // We have a delayed close.
            connect( socket, SIGNAL(delayedCloseFinished()),
                    SLOT(socketClosed()) );
        } else {
            // The socket is closed.
            socketClosed();
        }
    }


    void socketConnected()
    {
       emit connectedToTor();
    }

    void socketConnectionClosed()
    {
        emit torConnectionClosed();
    }

    void socketClosed()
    {
    }

    void socketError( int e )
    {
         if ( e == QSocket::ErrHostNotFound ||
              e == QSocket::ErrConnectionRefused )
            emit fatalError();
    }

    void parseEvent(const QString &type, const QString &info);
    void parseStream(const QString &info);
    void parseGuards(const QString &info);
    void parseORConn(const QString &info);
    void parseServer(const QString &info);
    void parseCircuit(const QString &info);
    void parseInfo(const QString &type, const QString &info);
    void parseBW(const QString &info);
    void parseAddrmap(const QString &info);
    void parseStreamBW(const QString &info);
    void parseDirStatus(const QString &info);
    void parseStatusGeneral(const QString &info);
    void resetClientReport();
    void updateCandidateServers(const QString &path);
    void handle250(const QString &line);
    void updateServerReport(const QString &message,
                            const QString &info);
    void updateClientReport(const QString &message);
    void populateMessageFromStatusDetail(const QString &line,
                                         QString &message);
    void handleTorVersion( const QString &caption);
    void readRouters();

private:
    QSocket *socket;
    bool m_expectingCircuitInfo;
    bool m_expectingStreamInfo;
    bool m_expectingOrconnInfo;
    bool m_expectingGuardsInfo;
    bool m_expectingDirStatus;
    bool m_expectingServerInfo;
    bool m_resolvingServerAddress;
    bool geoip_db;
    QString m_currentTorVersion;
    bool elementShouldBeUsed(const KConfigSkeletonItem* it);
    bool noSpecialProcessing(const KConfigSkeletonItem* it);
    bool readCookie();
    QString ds_identity;
    QString ds_fp_identity;
    QString ds_ip;
    QString ds_server;
    QString ds_date;
    QString ds_statuses;
    QString m_statustip;
    QString m_statustiptmp;
    bool m_controllerWorking;
    bool authorityread;
    bool m_firstloadcomplete;
    bool m_firstdircomplete;
    PrevConfig::PrevConfigList m_previtems;
    int m_CurBandwidthRate;
    int m_CurBandwidthBurst;
    int m_CurMaxAdvertisedBandwidth;
    QString m_statustipIP;
    typedef QMap<QString, QString> statusmap;
    statusmap statusMessage;
    statusmap serverStatus;
    statusmap serverStatusIcon;
    statusmap clientStatus;
    statusmap portMessage;
    statusmap clientStatusIcon;
    QStringList serverReport;
    QStringList clientReport;
    QStringList m_WarnedPorts;
    QStringList m_portsToReject;
    QString m_portToRemove;
    QStringList logResolveList;
};

#endif //


