/***************************************************************************
 * $Id: torkactivelabel.h,v 1.4 2008/07/31 19:56:27 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/


#ifndef _TORKACTIVELABEL_H_
#define _TORKACTIVELABEL_H_

#include <kactivelabel.h>


class TorkActiveLabel : public KActiveLabel
{
Q_OBJECT

public:
    TorkActiveLabel(const QString &text, QWidget *parent, const char *name=0);
    TorkActiveLabel(QWidget *parent, const char *name=0);
    QPoint m_mousepos;
    QPoint getMousePos();

protected:
    void contentsMouseMoveEvent( QMouseEvent *e );
    void contentsMousePressEvent( QMouseEvent *e );
    void contentsMouseReleaseEvent( QMouseEvent *e );
    
private:
    QPoint presspos;
    bool mousePressed;
    int dropTimer;
    bool m_recentDrop;
    QFont thisfont;


};
#endif // _TORKVIEW_H_
