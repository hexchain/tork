/***************************************************************************
** $Id: questions.h,v 1.35 2008/08/14 21:17:39 hoganrobert Exp $
 *
 *   Based on config method in Tor
 *
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
#include "tork.h"
#include <qstring.h>


/** A variable allowed in the configuration file or on the command line. */
typedef struct question_t {
  const char *logquestion; /**< The full keyword (case insensitive). */
  const QString headline; /**< String (or null) describing initial value. */
  const QString body; /**< String (or null) describing initial value. */
  char torsaid[250]; /**< String (or null) describing initial value. */
  const QString question; /**< String (or null) describing initial value. */
  display_status_t state;  /**< The type of the value. */
  bool persistent;
  bool showstopper;
  const QString type; /**< String (or null) describing initial value. */
  const char *icon; /**< String (or null) describing initial value. */
  bool (tork::*pt2Member2)();
  void (tork::*silentAction)();
  void (tork::*pt2Member)();
} question_t;


/** An entry for config_vars: "The option <b>name</b> has type
 * CONFIG_TYPE_<b>conftype</b>, and corresponds to
 * or_options_t.<b>member</b>"
 */
#define MSG(logquestion,headline,body,torsaid,question, persistent, \
            showstopper, state,  type, icon, show, silentAction, doIt) \
  { logquestion, headline,body,torsaid,question, persistent, showstopper, \
    state,  type, icon, show, silentAction, doIt }

/** Array of configuration options.  Until we disallow nonstandard
 * abbreviations, order is significant, since the first matching option will
 * be chosen first.
 */
static question_t _tork_questions[] = {
    MSG("Your ContactInfo config option is not set", 
        i18n("You Are Running A Server Without Any Contact Information!"),
        i18n("You can set your contact info in the configuration section 'My Server'. Please do so! "),
        "",
        i18n("Would you like to set your contact info now?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show TorK Feedback"),
        "tork_alert",
        (&tork::contactInfoSet),
        (&tork::dummyAction),
        (&tork::enterContactInfo)),

    MSG("Could not bind to", 
        i18n("Tor Couldn't Bind to One of the Addresses/Ports you configured!"),
        i18n("Tor is probably already running. If you like, TorK can connect to the already-running instance of Tor and manage that for you instead. (You will have to open the configuration dialog and re-apply any settings you wished to use.) "),
        "",
        i18n("Would you like to do this now?"),
        DISPLAY_NONE,
        true,
        true,
        i18n("Show TorK Feedback"),
        "tork_alert",
        (&tork::showGuideQuestions),
        (&tork::dummyAction),
        (&tork::fixAddressPort)),
    MSG("has not managed to confirm that its ORPort is reachable", 
        i18n("Sorry! Your Tor Server is not working!"),
        i18n("You could be blocking incoming traffic on your Tor port."),
        "",
        i18n("Would you like to see information on how to fix this?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show TorK Feedback"),
        "tork_alert",
        (&tork::showGuideQuestions),
        (&tork::retryUPnP),
        (&tork::serverHelp)),
    MSG("has not managed to confirm that its DirPort is reachable", 
        i18n("Sorry! Your Tor Directory Server is not working! You can't share your copy of the network directory with other servers."),
        i18n("You could be blocking incoming traffic on your Tor port."),
        "",
        i18n("Would you like to see information on how to fix this?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show TorK Feedback"),
        "tork_alert",
        (&tork::showGuideQuestions),
        (&tork::retryUPnP),
        (&tork::serverHelp)),
    MSG("must have public IP addresses", 
        i18n("Sorry! You can't run a Tor Server!"),
        i18n("You appear to be behind a NAT router and TorK/Tor can't determine your public IP address."),
        "",
        i18n("Would you like to continue running as a client only?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show TorK Feedback"),
        "tork_alert",
        (&tork::showGuideQuestions),
        (&tork::dummyAction),
        (&tork::continueAsClient)),
    MSG("Our testing circuit (to see if your ORPort is reachable) has failed.", 
        i18n("A Test Connection to Your Server Failed!"),
        i18n("The address/port you specified in 'My Server' is proving difficult to connect to! Is it your firewall maybe?"),
        "",
        i18n("Would you like to see information on how to fix this?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show TorK Feedback"),
        "tork_alert",
        (&tork::showGuideQuestions),
        (&tork::retryUPnP),
        (&tork::serverHelp)),
    MSG("Please upgrade!", 
        i18n("Your Version of Tor is a Bit Out-of-Date!"),
        i18n("TorK can download and compile the latest stable version for you."),
        "",
        i18n("Would you like to try this?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show TorK Feedback"),
        "tork_torlogo",
        (&tork::showGuideQuestions),
        (&tork::dummyAction),
        (&tork::updateTorStable)),
    MSG("cannotcontacttor", 
        i18n("TorK cannot connect to Tor!"),
        i18n("If you are trying to manage a remote or already-running instance of Tor you may not have configured the address and/or port of the Tor server correctly."),
        "",
        i18n("Would you like to configure the address and port now?"),
        DISPLAY_NONE,
        false,
        true,
        i18n("Show TorK Feedback"),
        "tork_alert",
        (&tork::showGuideQuestions),
        (&tork::dummyAction),
        (&tork::quickConfig)),

    MSG("applysettings", 
        i18n("Would you like to apply your settings to Tor?"),
        i18n("You are connecting to a remote or local instance of Tor, it may not have the settings you've configured with TorK."),
        "",
        i18n("Would you like to apply the settings now? (Note that you can do this automatically in future by selecting the option in the 'Quick Configure' dialog.)"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Alway Ask Before Applying Settings "),
        "tork_help",
        (&tork::showApplySettingsQuestions),
        (&tork::dummyAction),
        (&tork::applySettingsToRunningTor)),

    MSG("securitynotice", 
        i18n("Your Traffic Can Be Eavesdropped!"),
        "",
        "",
        i18n("Try to use the secure version of services (e.g. https: instead of http:) if you are entering a username and password or the content is sensitive. Would you like to see an explanation of why using Tor can make un-encrypted traffic <b>potentially less secure</b> than normal?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show Security Warnings"),
        "tork_info",
        (&tork::showSecurityWarnings),
        (&tork::dummyAction),
        (&tork::readEavesdropping)),

    MSG("privoxynotrunning", 
        i18n("Are you sure your Privacy Proxy is running?"),
        i18n("TorK tested your configured privacy proxy and it does not seem "
              "to be running."),
        "",
        i18n("Would you like TorK to use Privoxy instead?"),
        DISPLAY_NONE,
        true,
        false,
        i18n("Show Security Warnings"),
        "tork_alert",
        (&tork::showSecurityWarnings),
        (&tork::dummyAction),
        (&tork::letTorKManagePrivoxy)),

    MSG("privoxycouldntstart", 
        i18n("TorK couldn't start your Privacy Proxy!"),
        i18n("This may be because you have configured it to launch at system startup. If that is the case, and you have reason to believe it is configured to listen to Tor, then just click 'No' and take a look at the 'Konqueror' settings in the configuration dialog."),
        "",
        i18n("Would you like TorK to try restarting it again?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show Security Warnings"),
        "tork_alert",
        (&tork::showSecurityWarnings),
        (&tork::dummyAction),
        (&tork::startPrivoxy)),

    MSG("privoxydied", 
        i18n("Your Privacy Proxy just stopped working!"),
        i18n("It may have crashed."),
        "",
        i18n("Would you like TorK to restart it again?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show Security Warnings"),
        "tork_alert",
        (&tork::showSecurityWarnings),
        (&tork::dummyAction),
        (&tork::startPrivoxy)),

    MSG("screamingnotice", 
        i18n("No! No! Won't Someone Please Think Of The Children!?"),
        "",
        "",
        i18n("Would you like to see an explanation of why this is <b>absolutely the wrong thing to do</b> even by normal standards?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show Security Warnings"),
        "tork_alert",
        (&tork::showSecurityWarnings),
        (&tork::dummyAction),
        (&tork::readEavesdropping)),

    MSG("needalpha", 
        i18n("This version of TorK needs the most recent unstable version of Tor!"),
        i18n("You can still use TorK with this version of Tor, but the experience may be sub-optimal!"),
        "",
        i18n("Would you like to download the most recent alpha version and use that?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show Security Warnings"),
        "tork_alert",
        (&tork::showSecurityWarnings),
        (&tork::dummyAction),
        (&tork::updateTorUnstable)),

    MSG("DANGEROUS_VERSION", 
        i18n("You have an unrecommended version of Tor!"),
        i18n("You can still use TorK with this version of Tor, but the experience may be sub-optimal!"),
        "",
        i18n("Would you like to download the most recent stable version and use that?"),
        DISPLAY_NONE,
        false,
        false,
        i18n("Show Security Warnings"),
        "tork_alert",
        (&tork::showSecurityWarnings),
        (&tork::dummyAction),
        (&tork::updateTorStable)),

    MSG("DANGEROUS_PORT", 
        i18n("You could leak password information to Tor operators!"),
        i18n("This port is unencrypted and you could give away sensitive information!"),
        "",
        i18n("If you are sure you are comfortable with this, click 'Yes' and "
             "Tor will allow traffic on these ports for the rest of this session."),
        DISPLAY_NONE,
        true,
        false,
        i18n("Show Security Warnings"),
        "tork_alert",
        (&tork::showSecurityWarnings),
        (&tork::assignPortToRemove),
        (&tork::allowPlainTextPorts)),

    MSG("filterfailed", 
        i18n("One or More FailSafes Were Not Applied!"),
        i18n("There was an error when applying your failsafe request."),
        "",
        i18n("Would you like to view the failsafe rules?"),
        DISPLAY_NONE,
        true,
        false,
        i18n("Show TorK Feedback"),
        "tork_alert",
        (&tork::showGuideQuestions),
        (&tork::dummyAction),
        (&tork::quickConfig)),

    MSG("cookierequired", 
        i18n("You need to use a cookie to connect to Tor!"),
        i18n("TorK can look up the cookie and attempt to use it."),
        "",
        i18n("Would you like TorK to attempt connecting with a cookie?"),
        DISPLAY_NONE,
        true,
        true,
        i18n("Show TorK Feedback"),
        "tork_alert",
        (&tork::showGuideQuestions),
        (&tork::dummyAction),
        (&tork::reconnectWithCookie)),

    MSG("cookienotfound", 
        i18n("Tor's Authentication Cookie Not Available!"),
        i18n("The cookie may be stored in a location that you do not have permission to access. TorK can run a script as 'root' and attempt to copy the cookie to an accessible location. You can then try connecting to Tor again."),
        "",
        i18n("Would you like do this? (You will be asked for the 'root' password."),
        DISPLAY_NONE,
        true,
        true,
        i18n("Show TorK Feedback"),
        "tork_alert",
        (&tork::showGuideQuestions),
        (&tork::dummyAction),
        (&tork::copyCookie)),

    MSG("jdsklajdkslajdskla",
        i18n("Tor Controller is Not Responding!"), 
        i18n("TorK hasn't been able to contact Tor yet. "),
        "",
        i18n("Would you like to quit the connection attempt?"),
        DISPLAY_NONE,
        true,
        false,
        i18n("Show TorK Feedback"),
        "tork_alert",
        (&tork::showGuideQuestions),
        (&tork::dummyAction),
        (&tork::stopTor)),

    MSG("terminatetor",
        i18n("Tor will close gracefully in 30 seconds!"),
        i18n("The delay allows other Tor users to re-route their connections to other servers."),
        "",
        i18n("Would you like to go ahead and shut down immediately? "),
        DISPLAY_NONE,
        true,
        true,
        i18n("Show TorK Feedback"),
        "tork_help",
        (&tork::showGuideQuestions),
        (&tork::dummyAction),
        (&tork::stopTor)),

     MSG("passwordlost",
         i18n("You used the wrong password to connect to Tor!"),
         i18n("Maybe TorK crashed and lost the temporary password for connecting to Tor?"),
         "",
         i18n("If Tor is running locally TorK can reset Tor and then retry the connection. Would you like TorK to try this (<b>it will need to ask for your root password</b>)? "),
         DISPLAY_NONE,
         true,
         true,
         i18n("Show TorK Feedback"),
         "tork_alert",
         (&tork::showGuideQuestions),
         (&tork::dummyAction),
         (&tork::resetTor)),


  { NULL, NULL, NULL, "", NULL, DISPLAY_NONE, false, false, NULL, NULL,
    NULL, NULL, NULL }
};
#undef MSG
