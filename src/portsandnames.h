/***************************************************************************
** $Id: portsandnames.h,v 1.3 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   Based on config method in Tor
 *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

typedef struct portsandnames_t {
  const char *port; /**< The full keyword (case insensitive). */
  const char *name; /**< String (or null) describing initial value. */
} portsandnames_t;


#define MSG(port,icon)                             \
  { port,icon }

static portsandnames_t _port_name[] = {
    MSG("21","ftp"),
    MSG("22","ssh"),
    MSG("23","telnet"),
    MSG("5050","msn"),
    MSG("5190","aol"),
    MSG("5222","jabber"),
    MSG("6667","irc"),
  { NULL, NULL }
};
#undef MSG

