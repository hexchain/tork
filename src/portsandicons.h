/***************************************************************************
** $Id: portsandicons.h,v 1.15 2008/07/31 19:56:26 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   Based on config method in Tor
 *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

/** Enumeration of types which option values can take */
typedef enum display_once_t {
  DISPLAY = 0,   /**< An arbitrary string. */
  DONT_DISPLAY_AGAIN,         /**< A non-negative integer less than MAX_INT */
} display_once_t;

typedef struct portsandicons_t {
  const char *port; /**< The full keyword (case insensitive). */
  const char *icon; /**< String (or null) describing initial value. */
  int secure; /**< String (or null) describing initial value. */
  int screaminglyinsecure; /**< String (or null) describing initial value. */
  display_once_t display; 
} portsandicons_t;


#define MSG(port,icon,secure, screaminglyinsecure, display)                             \
  { port,icon,secure, screaminglyinsecure,display }

static portsandicons_t _port_icon[] = {
    MSG("80","konqueror", 0,0 , DISPLAY),
    MSG("443","tork_konqueror_https",1 ,0 , DISPLAY),
    MSG("http","konqueror",0 ,0 , DISPLAY),
    MSG("https","tork_konqueror_https",1 ,0 , DISPLAY),
    MSG("21","ftp",0 , 1 ,DISPLAY),
    MSG("22","tork_konsole_https",1 ,0 , DISPLAY),
    MSG("ssh","tork_konsole_https",1 ,0 , DISPLAY),
    MSG("telnet","konsole",0 ,1 , DISPLAY),
    MSG("23","konsole",0 ,1 , DISPLAY),
    MSG("25","tork_mail",0 ,1 , DISPLAY),
    MSG("smtp","tork_mail",0 ,1 , DISPLAY),
    MSG("53","error",0 ,0 , DISPLAY),
    MSG("domain","error",0 ,0 , DISPLAY),
    MSG("110","tork_mail",0 ,1 , DISPLAY),
    MSG("pop3","tork_mail",0 ,1 , DISPLAY),
    MSG("1863","tork_msn_protocol",0 ,0 , DISPLAY),
    MSG("mmcc","tork_yahoo_protocol",0 ,0 , DISPLAY),
    MSG("5050","tork_yahoo_protocol",0 ,0 , DISPLAY),
    MSG("6667","tork_irc_protocol",0 ,0 , DISPLAY),
    MSG("ircd","tork_irc_protocol",0 ,0 , DISPLAY),
    MSG("9030","tork_tor",1 , 0 ,DISPLAY),
    MSG("9031","tork_tor",1 , 0 ,DISPLAY),
    MSG("9032","tork_tor",1 , 0 ,DISPLAY),
    MSG("9001","tork_tor",1 , 0 ,DISPLAY),
    MSG("9051","tork_tor",1 , 0 ,DISPLAY),
    MSG("9050","tork_tor",1 , 0 ,DISPLAY),
  { NULL, NULL, 0, 0, DISPLAY }
};
#undef MSG

