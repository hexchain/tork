/***************************************************************************
 *   Copyright (C) 2005 Novell, Inc.                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA           *
 ***************************************************************************/

#include "hitwidget.h"
#include "kerrylabel.h"

#include <kpushbutton.h>
#include <kdebug.h>
#include <kiconloader.h>
#include <qtoolbutton.h>
#include <klocale.h>
#include <kurllabel.h>
#include <kglobalsettings.h>
#include <qlayout.h>
/*#include <konq_filetip.h>*/
#include <qscrollview.h>
#include <qcombobox.h>

HitWidget::HitWidget(QString uri, QString mimetype, KWidgetListbox *parent, const char *name)
  : HitWidgetLayout(parent, name), m_uri(uri), m_mimetype(mimetype), m_collapsed(false),
    m_was_collapsed(false), m_icon(QString::null)
{
  HitWidgetLayoutLayout->setMargin(4);
  toolButton1->setEnabled(false);
  toolButton1->hide();
  score->setHidden(true);
  //score->setText("");
  setDescriptionText("");
  setPropertiesText("");
  icon->installEventFilter(this);
/*  pFileTip = new KonqFileTip(parent);*/
/*  pFileTip->setItem(0L);*/
  qsv=parent;

}

HitWidget::~HitWidget()
{
/*  delete pFileTip;*/
}

void HitWidget::setIcon(const QString name)
{
  m_icon = name;
  if (m_collapsed)
    icon->setPixmap(KGlobal::iconLoader()->loadIcon(m_icon, KIcon::NoGroup, KIcon::SizeSmall));
  else
    icon->setPixmap(KGlobal::iconLoader()->loadIcon(m_icon, KIcon::NoGroup, KIcon::SizeLarge));
}

void HitWidget::toggleCollapsed()
{
  setCollapsed(!m_collapsed);
}

void HitWidget::setCollapsed(bool collapsed)
{
  if (m_collapsed==collapsed)
    return;

  if (collapsed) {
    toolButton1->setEnabled(true);
    m_was_collapsed = true;
    icon->setPixmap(KGlobal::iconLoader()->loadIcon(m_icon, KIcon::NoGroup, KIcon::SizeSmall));
#if 0
    i18n("Expand");
    i18n("Collapse");
    i18n("Expand All");
    i18n("Collapse All");
    i18n("(still searching)");
#endif
    toolButton1->setIconSet(SmallIconSet("info"));
    description->setHidden(true);
    properties->setHidden(true);
    score->setHidden(true);
    icon->setMinimumSize( QSize( 64, 16 ) );
    icon->setMaximumSize( QSize( 64, 16 ) );
    m_collapsed = collapsed;
  }
  else {
    icon->setPixmap(KGlobal::iconLoader()->loadIcon(m_icon, KIcon::NoGroup, KIcon::SizeLarge));
    toolButton1->setIconSet(SmallIconSet("2uparrow"));
    description->setHidden(false);
    properties->setHidden(false);
    //score->setHidden(false);
    icon->setMinimumSize( QSize( 64, 64 ) );
    icon->setMaximumSize( QSize( 64, 64 ) );
    m_collapsed = collapsed;
    emit uncollapsed(this);
  }

  if (qsv)
     qsv->adjustSize(this);
}

bool HitWidget::isCollapsed() const
{
  return m_collapsed;
}

void HitWidget::setDescriptionText(QString text)
{
  description->setText(text);
}

void HitWidget::setPropertiesText(QString text)
{
  properties->setText(text);
}

void HitWidget::insertHeaderWidget( int index, QWidget * widget)
{
  layoutHeader->insertWidget(index,widget);
}

void HitWidget::insertHitWidget( int index, QWidget * widget)
{
  layoutButton->insertWidget(index,widget);
}

void HitWidget::insertTextWidget( int index, QWidget * widget)
{
  layoutText->insertWidget(index,widget);
}

void HitWidget::insertHitSpacing( int index, int size)
{
  layoutButton->insertSpacing(index,size);
}

QString HitWidget::uri() const
{
  return m_uri;
}

void HitWidget::setUri(const QString uri)
{
/*  pFileTip->setItem(0L);*/
  m_uri = uri;
}

QString HitWidget::mimetype() const
{
  return m_mimetype;
}

void HitWidget::adjustSize()
{
  int dwidth, pwidth;

  HitWidgetLayout::adjustSize();
  if (m_was_collapsed) {
     dwidth = width()-160;
     pwidth = width()-160;
  }
  else {
     dwidth = description->size().width() + 160;
     pwidth = properties->size().width() + 160;
  }

  description->setFixedSize(dwidth,description->heightForWidth(dwidth));
  properties->setFixedSize(pwidth,properties->heightForWidth(pwidth));
  HitWidgetLayout::adjustSize();
}

bool HitWidget::eventFilter( QObject *, QEvent * )
{
/*  if ( obj == icon && !m_uri.isEmpty() ) {
    if ( ev->type() == QEvent::Enter && parent() ) {
      pFileTip->setOptions(true, true, 6);
      KFileItem *fileitem=new KFileItem(m_uri,m_mimetype,KFileItem::Unknown);
      QPoint viewport = qsv->viewport()->mapFromGlobal(mapToGlobal(icon->pos()));
      QRect qr(qsv->viewportToContents(viewport),QSize(icon->width()*2,icon->height()));
      pFileTip->setItem(fileitem,qr,icon->pixmap());
    }
    else if ( ev->type() == QEvent::Leave )
      pFileTip->setItem(0L);
    return HitWidgetLayout::eventFilter( obj, ev );
  }*/
  return false;
}

void HitWidget::changeTerminalUrl(int ) {

    QString app = score->currentText();
    QString title = shellTitles[score->currentText()];

    icon->setURL(QString("%1 %2").arg(app).arg(title));

}

#include "hitwidget.moc"
