/***************************************************************************
 ** $Id: main.cpp,v 1.61 2010/07/24 12:24:04 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/


#include "tork.h"
#include "crashhandler.h"
#include "torkconfig.h"
#include "functions.h"
#include "newfirstrunwizard.h"
#include <kapplication.h>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>
#include <kconfigdialogmanager.h>

static const char description[] =
    I18N_NOOP("<b>TorK - An Anonymity Manager for the KDE Desktop.</b>\n"
              "This product is produced independently from the Tor anonymity\n"
              "software and carries no guarantee from The Tor Project about\n"
              "quality, suitability or anything else.");

static const char version[] = "";
static const char title[] = "TorK 0.33";

static KCmdLineOptions options[] =
{
    { "+[URL]", I18N_NOOP( "Document to open." ), 0 },
    { "toggleKDE", I18N_NOOP("Toggle Anonymous KDE"), 0 },
    { "anonymousFirefox", I18N_NOOP("Launch Anonymous Firefox"), 0 },
    { "anonymousOpera", I18N_NOOP("Launch Anonymous Opera"), 0 },
    { "anonymousKonsole", I18N_NOOP("Launch Anonymous Konsole"), 0 },
    { "anonymousKopete", I18N_NOOP("Launch Anonymous Kopete"), 0 },
    { "anonymousPidgin", I18N_NOOP("Launch Anonymous Pidgin"), 0 },
    { "anonymousGaim", I18N_NOOP("Launch Anonymous Gaim"), 0 },
    { "anonymousKonversation", I18N_NOOP("Launch Anonymous Konversation"), 0 },
    { "anonymousEmail", I18N_NOOP("Launch Mixminion Interface"), 0 },

    { 0, 0, 0 }
};

int main(int argc, char **argv)
{
    KAboutData about("tork", I18N_NOOP(title), version, description,
    KAboutData::License_GPL, "(C) 2006 - 2008 Robert Hogan", 0, 0, "tork-users@lists.sf.net");
    about.addAuthor( "Robert Hogan", I18N_NOOP("Author and Maintainer"), "robert@roberthogan.net", "http://tork.anonymityanywhere.com" );
    about.addAuthor( "Corinna Habets", I18N_NOOP("Icons"), 0);
    about.addAuthor( "Contains code by: Matt Edman, Justin Hipple, Max Howell,", 0, "" );
    about.addAuthor( "                  Trolltech AS, Roger Dingledine, Nick Mathewson, ", 0, "" );
    about.addAuthor( "                  Sebastien Trueg, Joris Guisson,  Markus Gustavsson, ", 0, "" );
    about.addAuthor( "                  Diego Petena, Ben Burton, David Sansome ", 0, "" );
    about.addAuthor( "                  Stephan Binner, Hugo Parente Lima ", 0, "" );
    about.addAuthor( I18N_NOOP("This product includes GeoIP data created by MaxMind"),
                     0, 0, "http://maxmind.com/" );
    about.addAuthor( I18N_NOOP("The Tor(TM) trademark and Tor Onion Logo are trademarks of The Tor Project."),
                     0, 0, "http://www.torproject.org/" );


    about.addCredit( "Matthias Slovig", I18N_NOOP("Flag images by which can be used under this Creative Commons License: "
        "http://creativecommons.org/licenses/by/2.0/de/"), "matthias.slovig@baseball-in-bayern.de", "http://flags.blogpotato.de/");
    about.addCredit( "Emre Aladag, Inanc Yildirgan, Mustafa Gunay, Ertugrul Erata", I18N_NOOP("Turkish Translation"), 0);
    about.addCredit( "Liu Songhe", I18N_NOOP("Chinese Translation"), "shuizhuyuanluo@126.com");
    about.addCredit( "Marek Stopka", I18N_NOOP("Czech Translation"), "marekstopka@gmail.com");
    about.addCredit( "Hans-J. Ullrich", I18N_NOOP("German Translation"), "hans.ullrich@loop.de");
    about.addCredit( "Max Urgel", I18N_NOOP("German Translation"), "bitwisser@googlemail.com");
    about.addCredit( "Daniel Berthereau", I18N_NOOP("French Translation"),
                     "Daniel.Berthereau@free.fr");
    KCmdLineArgs::init(argc, argv, &about);
    KCmdLineArgs::addCmdLineOptions(options);
    KApplication app;


    //Check for the existence of anonymizable/helper programs first.
    QStringList programList;
    programList << "firefox" << "kopete" << "gaim" <<
                   "pidgin" << "opera" << "konversation"
                << "konsole" << "yakuake" << "xterm" << "konqueror"
                << "rxvt" << "gnome-terminal" << "gpg";
    TorkConfig::setAvailablePrograms(findPrograms(programList));

    //Run the wizard if necessary, and exit TorK completely if the wizard is
    //cancelled.
    if (TorkConfig::user().isEmpty()){
        FirstRunWizard wizard;
        wizard.setCaption( i18n( "First-Run Wizard" ));
        if (wizard.exec() == QDialog::Rejected) {
                TorkConfig::setUser("");
                return 0;
        }
    }

    // see if we are starting with session management
    tork *widget = new tork;
    if (app.isRestored())
        widget->hide();
    else
        widget->show();

    //KCrash::setCrashHandler( torK::Crash::crashHandler );

    return app.exec();
}

