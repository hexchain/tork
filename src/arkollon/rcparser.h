/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
#ifndef RCPARSER_H
#define RCPARSER_H

#include <qstringlist.h>
#include <qmap.h>

class RcParser
{
public:
	RcParser();
	~RcParser();
	
	void addSearchDir(QString dir);
	bool openFile(QString name);
	
	QStringList sectionList();
	void setSection(QString section);
	
	QString readString(QString key, QString def=QString::null);
	int readInt(QString key, int def=0);
	bool readBool(QString key, bool def=false);
	QStringList readList(QString key);
	
private:
	QStringList dirs;
	QString fileName;
	QString currentSection;
	QMap< QString, QMap< QString, QString> > sections;
};

#endif
