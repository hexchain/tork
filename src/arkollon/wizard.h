/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
#ifndef WIZARD_H
#define WIZARD_H

#include <wizardbase.h>
#include <qlistview.h>
#include <qprocess.h>
#include <qsettings.h>
#include <qiconview.h>
#include <qregexp.h>
#include <qvaluevector.h>
#include <qstring.h>
#include <stdio.h>

#include "logdialog.h"
class HeaderListItem;

struct Component
{
	QString name;
	QString niceName;
	QString subDir;
	QString forceDir;
	bool optional;
	bool kdeOnly;
	bool gnomeOnly;
	QString description;
	QString confOptions;
	
	QMap<QString, uint> buildTimes;
	
	bool alreadyInstalled;
};

class ComponentListItem : public QCheckListItem
{
public:
	ComponentListItem(struct Component c, QListView* parent);
	int compare(QListViewItem* i, int col, bool ascending) const;
	int rtti() const { return 1001; }
	
	struct Component component;
	int section;
};

class ActionFormat
{
public:
	ActionFormat() {}
	ActionFormat(QString a, QString t, QString r, int) { action=a; tool=t; regExp=r; }
	~ActionFormat() {}
	
	QString action;
	QString tool;
	QRegExp regExp;
};


class CompileError
{
public:
    CompileError() {}
    CompileError( const QString& type, const QString& message )
        : t( type ), m( message )
    { }

    QString type()   const          { return t; }
    QString message()   const          { return m; }
private:
    QString t;
    QString m;
};



class Wizard : public WizardBase
{
	Q_OBJECT
	
public:
	Wizard(QWidget *parent = 0, const char *name = 0);
	
	~Wizard();

	
private:
	void createActionFormats();
	void startProcess();
	void nextStep();
	void checkPassword();
	
	void updateTime(QString key);
	QString lastTimeLine;
	
	void logLine(QString line);
	void errorOccured();
	QString makeDirNice(QString name);
	
	QString sub(QString s);
	
	QString rootPassword;
	
private slots:
	void processExited();
	void readyReadStdout();
	void readyReadStderr();
	void kdeDirReady();
	void getInstalledComponents();
	void installedComponentsReady();
	
	void cancelPressed();
	void nextPressed();
	void previousPressed();
	void runPressed();
	void logPressed();
	void componentSelected(QListViewItem* item);
	void setProgress2Text(QString text);
	
	void setup();
	void setupFromRc();
	bool setupFromDir();

private:
	enum Stage
	{
		None,
		Autogen,
		Configure,
		Compile,
		Install,
		WriteUninstallInfo
	};
	
	QString dir;
	QStringList buildOrder;
	QValueList<struct Component> selectedComponents;
	uint totalBTime;
	uint elapsedBTime;
	uint elapsedTime;
	QTime timer;
	QProcess* externalProcess;
	QProcess* kdeDirProcess;
	QProcess* installedComponentsProcess;
	QString commandLine;
	bool currentIsQMake;
	uint currentComponent;
	enum Stage currentStage;
	bool needRoot;
	QString prefix;
	
	QStringList installedComponents;
	QString exec;
	QString desktop;
	
	QString kdeDir;
	
	LogDialog* logDialog;
	
	QMap<int, HeaderListItem*> headers;
	QValueList<ActionFormat> actionFormats;
	QStringList installedFiles;

};

#endif
