/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
#include "headerlistitem.h"

#include <qfontmetrics.h>
#include <qpainter.h>
#include <qapplication.h>

#include "uninstallwizard.h"
#include "wizard.h"

HeaderListItem::HeaderListItem(QListView* parent)
 : QListViewItem(parent)
{
}

int HeaderListItem::compare(QListViewItem* i, int , bool ) const
{
	switch (i->rtti())
	{
		case 1001: // Component
		{
			ComponentListItem* item = (ComponentListItem*) i;
			if (section > item->section)
				return 1;
			return -1;
		}
		
		case 1003: // App
		{
			AppListItem* item = (AppListItem*) i;
			if (section > item->section)
				return 1;
			return -1;
		}
		
		case 1002: // Header
		{
			HeaderListItem* item = (HeaderListItem*) i;
			if (section > item->section)
				return 1;
			if (section < item->section)
				return -1;
			return 0;
		}
	}
	return 0;
}

void HeaderListItem::paintCell(QPainter* p, const QColorGroup& cg, int , int width, int )
{
	p->fillRect(0, 0, width, height(), cg.base());
	
	QFont boldFont = p->font();
	boldFont.setBold(true);
	p->setFont(boldFont);
	p->drawText(listView()->itemMargin(), listView()->itemMargin(), width, QFontMetrics(boldFont).height(), Qt::AlignLeft, text(0));
	
	int textWidth = QFontMetrics(boldFont).width(text(0));
	p->fillRect(0, height() - 4 - listView()->itemMargin(), textWidth-10, 4, cg.highlight());
	
	QColor ca = cg.highlight();
	QColor cb = cg.base();
	// Taken from KPixmapEffect::gradient
	int rDiff, gDiff, bDiff;
	int rca, gca, bca /*, rcb, gcb, bcb*/;
	
//	register int x, y;
	
	rDiff = (/*rcb = */ cb.red())   - (rca = ca.red());
	gDiff = (/*gcb = */ cb.green()) - (gca = ca.green());
	bDiff = (/*bcb = */ cb.blue())  - (bca = ca.blue());
	
	register int rl = rca << 16;
	register int gl = gca << 16;
	register int bl = bca << 16;
	
	int rcdelta = ((1<<16) / 20) * rDiff;
	int gcdelta = ((1<<16) / 20) * gDiff;
	int bcdelta = ((1<<16) / 20) * bDiff;
	for( int x = textWidth-10; x < textWidth+10; x++)
	{
		rl += rcdelta;
		gl += gcdelta;
		bl += bcdelta;
	
		p->setPen(QColor(rl>>16, gl>>16, bl>>16));
		p->drawLine(x, height() - 4 - listView()->itemMargin(), x, height() - listView()->itemMargin() - 1);
	}
}

void HeaderListItem::paintFocus(QPainter* , const QColorGroup& , const QRect& )
{
}

int HeaderListItem::width(const QFontMetrics& , const QListView* lv, int ) const
{
	QFont boldFont = qApp->font();
	boldFont.setBold(true);
	QFontMetrics metrics(boldFont);
	return metrics.width(text(0)) + lv->itemMargin() + 10;
}

void HeaderListItem::setup()
{
	setHeight(qApp->fontMetrics().height() + listView()->itemMargin()*3 + 4);
}


