#include "data.h"

static struct EmbedImage {
    int width, height, depth;
    const unsigned char *data;
    int numColors;
    const QRgb *colorTable;
    bool alpha;
    const char *name;
} embed_image_vec[] = {
    { 16, 16, 32, (const unsigned char*)misc_data, 0, 0, TRUE, "misc" },
    { 130, 300, 32, (const unsigned char*)splash_data, 0, 0, FALSE, "splash" },
    { 32, 32, 32, (const unsigned char*)packageIcon_data, 0, 0, TRUE, "packageIcon" },
    { 0, 0, 0, 0, 0, 0, 0, 0 }
};

const QImage& qembed_findImage( const QString& name )
{
    static QDict<QImage> dict;
    QImage* img = dict.find( name );
    if ( !img ) {
	for ( int i = 0; embed_image_vec[i].data; i++ ) {
	    if ( strcmp(embed_image_vec[i].name, name.latin1()) == 0 ) {
		img = new QImage((uchar*)embed_image_vec[i].data,
			    embed_image_vec[i].width,
			    embed_image_vec[i].height,
			    embed_image_vec[i].depth,
			    (QRgb*)embed_image_vec[i].colorTable,
			    embed_image_vec[i].numColors,
			    QImage::BigEndian );
		if ( embed_image_vec[i].alpha )
		    img->setAlphaBuffer( TRUE );
		dict.insert( name, img );
		break;
	    }
	}
	if ( !img ) {
	    static QImage dummy;
	    return dummy;
	}
    }
    return *img;
}

