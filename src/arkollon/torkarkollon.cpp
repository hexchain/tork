/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/types.h> 

#include <qapplication.h>
#include "wizard.h"
#include "uninstallwizard.h"


int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	
	// Check if we're root
	if (getuid() != 0)
	{
		QString command = "kdesu -t -i tork -n -c \"";
		for (int i=0 ; i<app.argc() ; i++)
		{
			if (QString(app.argv()[i]).contains(" "))
				command += "\\\"" + QString(app.argv()[i]) + "\\\" ";
			else
				command += QString(app.argv()[i]) + " ";
		}
		command += "\"";
		return system(command.latin1());
	}
	
	WizardBase* wizard = NULL;
	
	for ( int i = 1; i < app.argc(); i++ )
	{
		if (QString(app.argv()[i]) == "--help")
		{
			printf("Arkollon is a utility to aid installation and removal of packages from source.\n");
			printf("Usage: arkollon DIRECTORY    Installs a source package from DIRECTORY\n");
			printf("       arkollon --uninstall  Starts the uninstaller\n");
			return 0;
		}
		if (QString(app.argv()[i]) == "--uninstall")
			wizard = new UninstallWizard(NULL);
	}
	
	if (!wizard)
		wizard = new Wizard(NULL);
	app.setMainWidget(wizard);
	
	return app.exec();
}
