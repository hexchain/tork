/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
#include "rcparser.h"

#include <qregexp.h>
#include <qfile.h>
#include <qtextstream.h>

RcParser::RcParser()
{
}


RcParser::~RcParser()
{
}

void RcParser::addSearchDir(QString dir)
{
	dirs.append(dir);
}

bool RcParser::openFile(QString name)
{
	// Check if it exists
	fileName = "";
	for ( QStringList::Iterator it = dirs.begin(); it != dirs.end(); ++it )
	{
		if (QFile::exists((*it) + "/" + name))
		{
			fileName = (*it) + "/" + name;
			break;
		}
	}
	
	if (fileName.isEmpty())
		return false;
	
	// Clear the current data
	sections.clear();
	
	// Read the file's contents
	QFile file(fileName);
	file.open(IO_ReadOnly);
	QTextStream stream(&file);
	
	QRegExp sectionRegExp("^\\[([^\\]]*)\\]$");
	QRegExp pairRegExp("^([^=\\s]*)([=\\s]*)(.*)$");
	currentSection = "RcParserDefaultSection";
	
	while (!stream.atEnd())
	{
		QString line = stream.readLine();
		if (line.left(1) == "#") // Comment
			continue;
		
		line = line.stripWhiteSpace();
		
		if (sectionRegExp.search(line) != -1)
		{
			currentSection = sectionRegExp.cap(1);
			//printf("Found section \"%s\"\n", currentSection.latin1());
			continue;
		}
		if (pairRegExp.search(line) != -1)
		{
			QString key = pairRegExp.cap(1);
			QString value = pairRegExp.cap(3);
			sections[currentSection][key] = value;
			//printf("Found pair \"%s\" = \"%s\"\n", key.latin1(), value.latin1());
			continue;
		}
		
		// Parse error, ignore the line
	}
	
	currentSection = "RcParserDefaultSection";
	return true;
}

void RcParser::setSection(QString section)
{
	currentSection = section;
}

QStringList RcParser::sectionList()
{
	return sections.keys();
}

QString RcParser::readString(QString key, QString def)
{
	QString ret = sections[currentSection][key];
	if (ret.isEmpty())
		return def;
	return ret;
}

int RcParser::readInt(QString key, int def)
{
	bool ok;
	int ret = sections[currentSection][key].toInt(&ok);
	if (!ok)
		return def;
	return ret;
}

bool RcParser::readBool(QString key, bool def)
{
	bool ret = def;
	if (sections[currentSection][key].lower() == "true")
		ret = true;
	if (sections[currentSection][key].lower() == "false")
		ret = false;
	if (sections[currentSection][key] == "1")
		ret = true;
	if (sections[currentSection][key] == "0")
		ret = false;
	return ret;
}

QStringList RcParser::readList(QString key)
{
	return QStringList::split(",", sections[currentSection][key]);
}




