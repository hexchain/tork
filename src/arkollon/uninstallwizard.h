/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

#ifndef UNINSTALLWIZARD_H
#define UNINSTALLWIZARD_H

#include "wizardbase.h"
#include "logdialog.h"
#include "headerlistitem.h"

#include <qlistview.h>
#include <qprocess.h>
#include <qpixmap.h>

class AppListItem : public QCheckListItem
{
public:
	
	AppListItem(QString nN, QString n, QListView* lv);
	int compare(QListViewItem* i, int col, bool ascending) const;
	int rtti() const { return 1003; }
	
	QString niceName;
	QString name;
	int section;
};



class UninstallWizard : public WizardBase
{
	Q_OBJECT

public:
	UninstallWizard(QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
	~UninstallWizard();

public slots:
	virtual void          logPressed();
	virtual void          cancelPressed();
	virtual void          previousPressed();
	virtual void          nextPressed();
	
	void processExited();
	void readyReadStdout();
	void readyReadStderr();
	
private:
	void checkPassword();
	void removeUserPackages();
	void removeGlobalPackages();
	void finished();
	
private:
	enum
	{
		ListingPackages,
		ListingFiles,
		RemovingGlobal
	} currentStage;
	
	LogDialog* logDialog;
	QProcess* externalProcess;
	HeaderListItem* globalHeader;
	
	QPixmap icon;
};

#endif

