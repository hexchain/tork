/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/
#ifndef HEADERLISTITEM_H
#define HEADERLISTITEM_H

#include <qlistview.h>

class HeaderListItem : public QListViewItem
{
public:
	HeaderListItem(QListView* parent);
	int compare(QListViewItem* i, int col, bool ascending) const;
	void paintCell(QPainter* p, const QColorGroup& cg, int column, int width, int align);
	void paintFocus(QPainter* p, const QColorGroup& cg, const QRect& r);
	void setup();
	int width(const QFontMetrics& fm, const QListView* lv, int c) const;
	int rtti() const { return 1002; }
	
	int section;
};

#endif
