#include "data.h"

void WizardBase::setAppName( QString n )
{
 QPixmap tmp;
 tmp.convertFromImage(qembed_findImage("packageIcon"));
 appIcon->setPixmap(tmp);
 tmp.convertFromImage(qembed_findImage("splash"));
 splash->setPixmap(tmp);
 
 setCaption("Installing "+n);
 titleLabel->setText("<b>Installing "+n+"</b>");
 appNameLabel->setText("<b>"+n+"</b>");
 shortcutBox->setText("Place a shortcut to "+n+" on the desktop");
}


void WizardBase::nextPressed()
{

}


void WizardBase::previousPressed()
{

}


void WizardBase::cancelPressed()
{

}


void WizardBase::browsePressed()
{

}


void WizardBase::runPressed()
{

}


void WizardBase::logPressed()
{

}



void WizardBase::componentSelected( QListViewItem * )
{

}
