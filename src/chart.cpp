/***************************************************************************
** $Id: chart.cpp,v 1.8 2008/07/31 19:56:25 hoganrobert Exp $
*   Copyright (C) 2006 - 2008 Robert Hogan                                *
*   robert@roberthogan.net                                                *
*                                                                         *
*   Copyright (C) 2005 by Hugo Parente Lima                               *
*   hugo_pl@users.sourceforge.net                                         *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
***************************************************************************/

#include "chart.h"
#include "functions.h"
#include <qpainter.h>
#include <qbrush.h>
#include <kdebug.h>

using namespace tk;

Chart::Chart(QWidget* parent, const double* uploadBuffer, const double* downloadBuffer, int bufferSize, const int* ptr, const double* maxspeed, const double* sys_uploadBuffer, const double* sys_downloadBuffer, int sys_bufferSize, const int* sys_ptr, const double* sys_maxspeed, bool gotEth0) : QWidget(parent), mUplBuffer(uploadBuffer), mDldBuffer(downloadBuffer), mBufferSize(bufferSize), mPtr(ptr), mMaxSpeed(maxspeed),sys_mUplBuffer(sys_uploadBuffer), sys_mDldBuffer(sys_downloadBuffer), sys_mBufferSize(sys_bufferSize), sys_mPtr(sys_ptr), sys_mMaxSpeed(sys_maxspeed),mGotEth0(gotEth0) {
	setWFlags(Qt::WNoAutoErase);
	setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

void Chart::paintEvent(QPaintEvent*) {
	QPainter paint(this);
	paint.setBackgroundColor(Qt::black);
	paint.setBackgroundMode(Qt::OpaqueMode);
	QBrush brush(QColor(0x33, 0x33, 0x33), CrossPattern);
	paint.fillRect(0, 0, width(), height(), brush);
	

 	const double step = width()/double(mBufferSize);
	const int HEIGHT = height() - 1;
	int x;
	int lastX = x = width();
	int lastRxY = HEIGHT - int(HEIGHT * (mDldBuffer[*mPtr]/(*mMaxSpeed)));
	int lastTxY = HEIGHT - int(HEIGHT * (mUplBuffer[*mPtr]/(*mMaxSpeed)));
	int count = 0;
	for (int i = *mPtr; count < mBufferSize; i--) {
		if (i < 0)
			i = mBufferSize-1;
		int rxY = HEIGHT - int(HEIGHT * (mDldBuffer[i]/(*mMaxSpeed)));
		int txY = HEIGHT - int(HEIGHT * (mUplBuffer[i]/(*mMaxSpeed)));
        paint.setPen(Qt::green);
		paint.drawLine(lastX, lastRxY, x, rxY);
		paint.setPen(Qt::red);
		paint.drawLine(lastX, lastTxY, x, txY);
		//qDebug("%d => %d", i, int(mSpeedHistoryRx[i]));
		lastX = x;
		lastRxY = rxY;
		lastTxY = txY;
		
		count++;
		x = width() - int(step*(count+1));
		
	}

	paint.setFont(QFont("Helvetica", 8));
	paint.setPen( Qt::red );
	paint.drawText( rect(), Qt::AlignTop, QString("Tor Tx: %1 ").arg(BytesPerSecToString(mUplBuffer[*mPtr])));
	paint.setPen( Qt::green );
	paint.drawText( rect(), Qt::AlignBottom, QString("Tor Rx: %1 ").arg(BytesPerSecToString(mDldBuffer[*mPtr])));

    if (mGotEth0){
        lastX = x = width();
        lastRxY = HEIGHT - int(HEIGHT * (sys_mDldBuffer[*sys_mPtr]/(*sys_mMaxSpeed)));
        lastTxY = HEIGHT - int(HEIGHT * (sys_mUplBuffer[*sys_mPtr]/(*sys_mMaxSpeed)));
        count = 0;
        for (int i = *sys_mPtr; count < sys_mBufferSize; i--) {
            if (i < 0)
                i = sys_mBufferSize-1;
            int rxY = HEIGHT - int(HEIGHT * (sys_mDldBuffer[i]/(*sys_mMaxSpeed)));
            int txY = HEIGHT - int(HEIGHT * (sys_mUplBuffer[i]/(*sys_mMaxSpeed)));
            paint.setPen(Qt::darkGreen);
            paint.drawLine(lastX, lastRxY, x, rxY);
            paint.setPen(Qt::darkRed);
            paint.drawLine(lastX, lastTxY, x, txY);
            //qDebug("%d => %d", i, int(mSpeedHistoryRx[i]));
            lastX = x;
            lastRxY = rxY;
            lastTxY = txY;
            
            count++;
            x = width() - int(step*(count+1));
            
        }

        QString TorTX = QString("Tor Tx: %1 ").arg(BytesPerSecToString(mUplBuffer[*mPtr]));
        QString SysTX = QString("Sys Tx: %1 ").arg(BytesPerSecToString(sys_mUplBuffer[*sys_mPtr]));
        QString TorRX = QString("Tor Rx: %1 ").arg(BytesPerSecToString(mDldBuffer[*mPtr]));
        QString SysRX = QString("Sys Rx: %1 ").arg(BytesPerSecToString(sys_mDldBuffer[*sys_mPtr]));
        //Paint Text Representation
        QRect first = paint.boundingRect( rect(), Qt::AlignTop, TorTX);
        first.moveLeft(first.width());
        first.setWidth(paint.boundingRect( rect(), Qt::AlignTop, SysTX).width());
        paint.setPen( Qt::red );
        paint.drawText( first, Qt::AlignTop, SysTX);
    
        first = paint.boundingRect( rect(), Qt::AlignBottom, TorRX);
        first.moveLeft(first.width());
        first.setWidth(paint.boundingRect( rect(), Qt::AlignTop, SysRX).width());
        paint.setPen( Qt::green );
        paint.drawText( first, Qt::AlignTop, SysRX);

    }


}

