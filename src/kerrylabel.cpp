/* This file is part of the KDE libraries
   Copyright (C) 1998 Kurt Granroth <granroth@kde.org>
   Copyright (C) 2000 Peter Putzer <putzer@kde.org>
   Copyright (C) 2005 Jaroslaw Staniek <js@iidea.pl>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "kerrylabel.h"
#include "hitwidget.h"

#include <qdragobject.h>
#include <kglobalsettings.h>
#include <kurl.h>
#include <krun.h>
/*
#include <konq_popupmenu.h>
#include <konqbookmarkmanager.h>
#include <knewmenu.h>
*/

KerryLabel::KerryLabel (QWidget* parent, const char* name)
  : KURLLabel (parent, name)
{
  dragInfo.state = diNone;
}

void KerryLabel::mousePressEvent(QMouseEvent* ev)
{
  if (!url().isEmpty())
  {
    if ( ev->button() == LeftButton)
    {
      dragInfo.state = diPending;
      dragInfo.start = ev->pos();
    }
    else if (ev->button() == RightButton)
    {
        ev->accept();
        //popupMenu( mapToGlobal(ev->pos()) );
        return;
    }
  }
  KURLLabel::mousePressEvent(ev);
}

void KerryLabel::mouseMoveEvent(QMouseEvent* ev)
{
  if (dragInfo.state == diPending) {
      int distance = KGlobalSettings::dndEventDelay();
      if ( ev->x() > dragInfo.start.x() + distance || ev->x() < dragInfo.start.x() - distance ||
           ev->y() > dragInfo.start.y() + distance || ev->y() < dragInfo.start.y() - distance) {
          doDrag();
      }
      return;
  }
  KURLLabel::mouseMoveEvent(ev);
}

void KerryLabel::mouseReleaseEvent(QMouseEvent* ev)
{
  if ( ev->button() == LeftButton)
  {
      dragInfo.state = diNone;
  }
  KURLLabel::mouseReleaseEvent(ev);
}

void KerryLabel::doDrag()
{
  dragInfo.state = diDragging;
  const KURL kuri = KURL(url());
  dragInfo.dragObject = new QTextDrag("'"+kuri.url().replace("file://",QString::null)+"'", this);
  dragInfo.dragObject->dragCopy();
  // Don't delete the QTextDrag object.  Qt will delete it when it's done with it.
}

// void KerryLabel::popupMenu( const QPoint &_global )
// {
//     KFileItem item( ((HitWidget*)parent())->uri(),((HitWidget*)parent())->mimetype(),KFileItem::Unknown);
//     KFileItemList _items;
//     _items.append( &item );
// 
//     const KURL kurl(url());
//     KActionCollection act(this);
// 
//     KonqPopupMenu * popupMenu = new KonqPopupMenu( KonqBookmarkManager::self(), _items,
//                                                    kurl, act, (KNewMenu*)NULL, this,
//                                                    item.isLocalFile() ? KonqPopupMenu::ShowProperties : KonqPopupMenu::NoFlags,
//                                                    KParts::BrowserExtension::DefaultPopupItems );
// 
//     if (popupMenu->count())
//       popupMenu->exec( _global );
//     delete popupMenu;
// }

#include "kerrylabel.moc"
