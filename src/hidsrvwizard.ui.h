/***************************************************************************
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

#include "tork.h"
#include "functions.h"
#include <qtimer.h>
#include <kmessagebox.h>
#include <cstdlib>

bool local;
QString thttpd;


void HidSrvWizard::init()
{
    local=false;
    setAppropriate ( LocalWebService, false );
    setAppropriate ( LocalWebServiceFiles, false );
    setAppropriate ( ServiceName, true );
    setAppropriate ( RedirectService, true );

    QString location = getenv("HOME");
    location += "/.tork";
    QDir torkdir(location);
    if (!torkdir.exists() && !torkdir.mkdir(location)){
        KMessageBox::information (this,QString("Couldn't create directory: %1. Check the permissions!").arg(location));
        reject();
    }


}
void HidSrvWizard::localWeb_toggled( bool state)
{



    if (state){
        const char *paths = ":/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin";
        thttpd = getFullLocation(paths,"thttpd");
   
        if (thttpd)
            setAppropriate ( LocalWebService, !state );
        else
            setAppropriate ( LocalWebService, state );
    }else
        setAppropriate ( LocalWebService, state );

    setAppropriate ( LocalWebServiceFiles, state );
    setAppropriate ( RedirectService, !state );
    local = state;


}


void HidSrvWizard::downloadThttpd_clicked()
{
    TorkUpdate* updater = new TorkUpdate(this);
    updater->downloadThttpd();

}


void
HidSrvWizard::showPage( QWidget *w ) //virtual
{
    QWizard::showPage( w );


    if (currentPage() == this->GatherDetails){
        if (local)
            emit createService(QString("%1/.tork/%2/").arg(getenv("HOME")).arg(Nick->text()), QString("%1 localhost:%2").arg(WebPort->text()).arg(localWebPort->text()));
        else
            emit createService(QString("%1/.tork/%2/").arg(getenv("HOME")).arg(Nick->text()), QString("%1 %2:%3").arg(NonWebPort->text()).arg(RedirectAddress->text()).arg(RedirectPort->text()));
        GatheringDetailsText->setText("Please wait while Tor attempts to create the service.");
        QTimer::singleShot( 3000, this, SLOT(checkServiceDetails()) );
        finishButton()->setEnabled(false);
    }

}

void
HidSrvWizard::checkServiceDetails()
{

    QString hostname;
    QFile inf(QString("%1/.tork/%2/hostname").arg(getenv("HOME")).arg(Nick->text()));
    if ( inf.open(IO_ReadOnly) ) {
        inf.readLine(hostname,50);
        inf.close();
    }else{
        GatheringDetailsText->setText("Failed. Click Back and Next to try again.");
        return;
    }

    hostname.replace("\n","");

    cancelButton()->setEnabled(false);

    QString serviceDetails = "Hidden Service Hostname: ";
    serviceDetails += hostname;
    serviceDetails += "<br>";
    if (local){
        serviceDetails += "Serving files in:";
        serviceDetails += FilesToServe->url();
        serviceDetails += "<br>";
        serviceDetails += "Public Facing Port:"; 
        serviceDetails += WebPort->text();
        serviceDetails += "<br>";
        serviceDetails += "Tor Facing Port:"; 
        serviceDetails += localWebPort->text();
        emit addService(Nick->text(),WebPort->text(),QString("localhost:%1").arg(localWebPort->text()),hostname,FilesToServe->url(),QString("%1/.tork/%2/").arg(getenv("HOME")).arg(Nick->text()));

    }else{
        serviceDetails += "Redirecting to:";
        serviceDetails += RedirectAddress->text();
        serviceDetails += "<br>";
        serviceDetails += "On Port:";
        serviceDetails  += RedirectPort->text();
        serviceDetails += "<br>";
        serviceDetails += "Serving Tor Network with Port:";
        serviceDetails  += NonWebPort->text();
        serviceDetails += "<br>";
        emit addService(Nick->text(),NonWebPort->text(),QString("%1:%2").arg(RedirectAddress->text()).arg(RedirectPort->text()),hostname,"",QString("%1/.tork/%2/").arg(getenv("HOME")).arg(Nick->text()));

    }


    GatheringDetailsText->setText(serviceDetails);
    finishButton()->setEnabled(true);

}

void
HidSrvWizard::accept()
{
    QDialog::accept();
}

void
HidSrvWizard::reject()
{
    QDialog::reject();
}


