/***************************************************************************
 * $Id: torkactivelabel.cpp,v 1.4 2008/07/31 19:56:27 hoganrobert Exp $
 *   Copyright (C) 2006 - 2008 Robert Hogan                                *
 *   robert@roberthogan.net                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

#include "torkactivelabel.h"
#include "kdebug.h"
#include <qapplication.h>
#include <qheader.h>
#include <kiconloader.h>
#include <klocale.h>
#include <kstandarddirs.h>

TorkActiveLabel::TorkActiveLabel( const QString &text,QWidget * parent, const char * name) :
    KActiveLabel( text,parent, name ), mousePressed( FALSE )
{


}

TorkActiveLabel::TorkActiveLabel( QWidget * parent, const char * name) :
    KActiveLabel( parent, name ), mousePressed( FALSE )
{


}



void TorkActiveLabel::contentsMousePressEvent( QMouseEvent* e )
{
    thisfont = this->font();
    thisfont.setBold(true);
    this->setFont(thisfont);

    KActiveLabel::contentsMousePressEvent( e );
}

void TorkActiveLabel::contentsMouseMoveEvent( QMouseEvent* e )
{

    if (this->frameRect().contains(e->pos())){
        thisfont = this->font();
        thisfont.setUnderline(true);
        this->setFont(thisfont);
    }else{
        thisfont = this->font();
        thisfont.setUnderline(false);
        this->setFont(thisfont);

    }
    KActiveLabel::contentsMouseMoveEvent( e );
}

void TorkActiveLabel::contentsMouseReleaseEvent( QMouseEvent * e)
{
    thisfont = this->font();
    thisfont.setBold(false);
    this->setFont(thisfont);
    KActiveLabel::contentsMouseReleaseEvent(e);

}

QPoint TorkActiveLabel::getMousePos()
{
    return m_mousepos;
}

#include "torkactivelabel.moc"
