export CVS_RSH=ssh
export VN=0.33
export VER=tork-$VN
export TAG=v0_33
cd ..
TOPDIR=$PWD
cd TorkReleases
INSTALLDIR=$PWD


#create source package
cvs -z3 -d:ext:hoganrobert@tork.cvs.sourceforge.net:/cvsroot/tork export -r $TAG tork
cd tork
make -f Makefile.cvs
rm -rf autom4te.cache
cd ..
mv tork $VER
tar jcvf $VER.tar.bz2 $VER
tar zcvf $VER.tar.gz $VER
gpg -sba $VER.tar.bz2
gpg -sba $VER.tar.gz
